﻿using DataAccessLayer.Context;
using NUnit.Framework;

namespace DataAccessLayer.Tests
{
    [TestFixture]
    public class ApplicationContextTests
    {
        private string _connectionString;

        [SetUp]
        public void Initialize()
        {
            _connectionString = "mongodb+srv://Xaolas:za5xc6sd7vf8@cluster0-kqbxm.mongodb.net/test";
        }

        [Test]
        public void ConnectionToMongoDbShouldNotThrowException()
        {
            Assert.DoesNotThrow(() => new ApplicationContext(_connectionString));
        }
    }
}
