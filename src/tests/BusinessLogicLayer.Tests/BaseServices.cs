﻿using AutoMapper;
using BusinessLogicLayer.Dto;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Tests
{
    [TestFixture]
    public class BaseServices
    {
        [SetUp]
        public void Initialize()
        {
            Chats = DataInitializer.GetAllChats();
            Users = DataInitializer.GetAllUsers();
            Robots = DataInitializer.GetAllRobots();

            Mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<Chat, ChatDto>();
                cfg.CreateMap<Message, MessageDto>();
                cfg.CreateMap<Robot, RobotDto>();
            }));

            Uow = new Mock<IUnitOfWork>();
            Uow.Setup(x => x.Chats).Returns(() => GetRepository(Chats).Object);
            Uow.Setup(x => x.Users).Returns(() => GetRepository(Users).Object);
            Uow.Setup(x => x.Robots).Returns(() => GetCacheRepository(Robots).Object);
        }

        protected Mock<IUnitOfWork> Uow;
        protected IMapper Mapper;
        protected List<Chat> Chats;
        protected List<User> Users;
        protected ConcurrentDictionary<string, Robot> Robots;

        private Mock<IRepository<T>> GetRepository<T>(List<T> store) where T : EntityBase
        {
            var repo = new Mock<IRepository<T>>();
            repo.Setup(x => x.Create(It.IsAny<T>())).Callback<T>(store.Add);
            repo.Setup(x => x.Get(It.IsAny<string>())).Returns<string>(x =>
            {
                return store.FirstOrDefault(v => v.Id.Equals(x));
            });
            repo.Setup(x => x.Get(It.IsAny<Func<T, bool>>()))
                .Returns((Func<T, bool> expr) => store.FirstOrDefault(expr));
            repo.Setup(x => x.Delete(It.IsAny<string>())).Callback<string>(x =>
            {
                var value = store.FirstOrDefault(c => c.Id.Equals(x));
                if (value != null)
                    store.Remove(value);
                else
                    throw new Exception();
            });
            repo.Setup(x => x.GetAll(It.IsAny<Func<T, bool>>())).Returns((Func<T, bool> expr) => store.Where(expr));
            repo.Setup(x => x.GetAll()).Returns(store);
            return repo;
        }

        private Mock<ICacheRepository<T>> GetCacheRepository<T>(ConcurrentDictionary<string, T> store) where T : class
        {
            var repo = new Mock<ICacheRepository<T>>();
            repo.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<T>())).Returns<string, T>(store.TryAdd);
            repo.Setup(x => x.Exists(It.IsAny<string>())).Returns<string>(store.ContainsKey);
            repo.Setup(x => x.Get(It.IsAny<string>())).Returns<string>(x => store[x]);
            repo.Setup(x => x.Remove(It.IsAny<string>())).Returns<string>(x => store.TryRemove(x, out var value));
            repo.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<T>()))
                .Returns<string, T>((a, b) => store.TryUpdate(a, b, store[a]));
            return repo;
        }
    }
}