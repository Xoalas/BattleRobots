﻿using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using NUnit.Framework;
using System;
using System.Linq;

namespace BusinessLogicLayer.Tests
{
    [TestFixture]
    public class ChatServiceTests : BaseServices
    {
        [SetUp]
        public void ChatServiceInitialize()
        {
            _service = new ChatService(Uow.Object, Mapper);
        }

        private IChatService _service;

        [TestCase("1", 2)]
        [TestCase("3", 0)]
        public void ShouldReturnAllMessagesByChatId(string chatId, int expectedMsgNo)
        {
            var messages = _service.GetMessagesByChatId(chatId);

            Assert.DoesNotThrow(() => messages = _service.GetMessagesByChatId(chatId));
            Assert.AreEqual(expectedMsgNo, messages.Count());
        }

        [Test]
        public void ShouldCreateChat()
        {
            var expectedChatsCount = 4;
            var userId = "1";
            var oppId = "4";

            _service.CreateChat(userId, oppId);

            Assert.AreEqual(expectedChatsCount, Chats.Count());
        }

        [Test]
        public void ShouldCreateMessage()
        {
            var expectedMessagesCount = 3;
            var chatId = "1";
            var userId = "1";
            var text = "Text";

            _service.SendMessage(userId, chatId, text);

            Assert.AreEqual(expectedMessagesCount, Chats.FirstOrDefault(c => c.Id.Equals(chatId)).Messages.Count);
        }

        [Test]
        public void ShouldCreateNewChatWhenUsersDoesNotHaveAnyChats()
        {
            var expectedChatsCount = 4;
            var userId = "1";
            var enemyId = "4";

            _service.GetChatByUsers(userId, enemyId);

            Assert.AreEqual(expectedChatsCount, Chats.Count());
        }

        [Test]
        public void ShouldReturnAllChatsByUserId()
        {
            var userId = "1";
            var expectedChatsNumber = 2;

            var chats = _service.GetChatsByUserId(userId);

            Assert.AreEqual(expectedChatsNumber, chats.Count());
        }

        [Test]
        public void ShouldReturnChatByChatId()
        {
            var chatId = "1";

            var chat = _service.GetChat(chatId);

            Assert.AreEqual(chatId, chat.Id);
        }

        [Test]
        public void ShouldReturnChatByUsersId()
        {
            var userId = "1";
            var enemyId = "2";
            var expectedChatId = "1";

            var chat = _service.GetChatByUsers(userId, enemyId);

            Assert.AreEqual(expectedChatId, chat.Id);
        }

        [Test]
        public void ShouldReturnFalseIfUsersAlreadyHaveChat()
        {
            var userId = "1";
            var oppId = "2";

            var isCreated = _service.CreateChat(userId, oppId);

            Assert.IsFalse(isCreated);
        }

        [Test]
        public void ShouldReturnTrueIfChatCreatedForUsers()
        {
            var userId = "1";
            var oppId = "4";

            var isCreated = _service.CreateChat(userId, oppId);

            Assert.IsTrue(isCreated);
        }

        [Test]
        public void ShouldThrowExceptionWhenChatIdDoesNotExist()
        {
            var chatId = "-1";

            var ex = Assert.Throws<Exception>(() => _service.GetChat(chatId));

            Assert.That(ex.Message, Is.EqualTo($"Chat {chatId} not found"));
        }

        [Test]
        public void ShouldThrowExceptionWhenUserDoesNotExist()
        {
            var chatId = "1";
            var userId = "-1";
            var text = "Text";

            var ex = Assert.Throws<Exception>(() => _service.SendMessage(userId, chatId, text));

            Assert.That(ex.Message, Is.EqualTo($"User {userId} not found"));
        }

        [Test]
        public void ShouldThrowExceptionWhenUserIdDoesNotExist()
        {
            var userId = "-1";
            var oppId = "2";

            var ex = Assert.Throws<Exception>(() => _service.CreateChat(userId, oppId));

            Assert.That(ex.Message, Is.EqualTo($"User {userId} not found"));
        }
    }
}