﻿using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using NUnit.Framework;
using System;

namespace BusinessLogicLayer.Tests
{
    [TestFixture]
    public class RobotServiceTests : BaseServices
    {
        [SetUp]
        public void RobotServiceInitialize()
        {
            _service = new RobotService(Uow.Object, Mapper);
        }

        private IRobotService _service;

        [TestCase("ForceShield")]
        [TestCase("Maneuver")]
        [TestCase("Interferer")]
        [TestCase("Blaster")]
        [TestCase("RocketLauncher")]
        [TestCase("MachineGun")]
        [TestCase("Torpedoes")]
        public void ShouldReturnTrueIfEquipmentSetSuccessful(string equipment)
        {
            var userId = "1";

            var isShieldSetted = _service.Equip(userId, equipment);

            Assert.IsTrue(isShieldSetted);
        }

        [Test]
        public void CheckWinnerShouldReturnDrawWhenBothHealthLessOrEqualZero()
        {
            var userId = "1";
            var oppId = "2";

            Robots[userId].Health = -30;
            Robots[oppId].Health = 0;

            var response = _service.CheckWinner(userId, oppId);

            Assert.AreEqual("Draw", response);
        }

        [Test]
        public void CheckWinnerShouldReturnNothingWhenHealthGreaterThanZero()
        {
            var userId = "1";
            var oppId = "2";

            var response = _service.CheckWinner(userId, oppId);

            Assert.AreEqual(string.Empty, response);
        }

        [Test]
        public void CheckWinnerShouldReturnWinnerUserIdWhenOnlyOneHealthLessOrEqualZero()
        {
            var userId = "1";
            var oppId = "2";

            Robots[userId].Health = -30;
            Robots[oppId].Health = 100;

            var response = _service.CheckWinner(userId, oppId);

            Assert.AreEqual("2", response);
        }

        [Test]
        public void SetStatsShouldIncreaseVictoriesWinnerUserAndDecreaseDefeatsLoserUser()
        {
            var winnerId = "1";
            var loserId = "2";
            var expVictories = Users.Find(x => x.Id.Equals(winnerId)).Victories;
            var expDefeats = Users.Find(x => x.Id.Equals(loserId)).Defeats;

            _service.SetStats(winnerId, loserId);

            var victories = Users.Find(x => x.Id.Equals(winnerId)).Victories;
            var defeats = Users.Find(x => x.Id.Equals(loserId)).Defeats;

            Assert.IsTrue(expVictories + 1 == victories && expDefeats + 1 == defeats);
        }

        [Test]
        public void ShouldDecreaseHealthIfWrongShildIsSetted()
        {
            var userId = "1";
            var oppId = "2";
            var weapon = "Blaster";
            var shield = "Interferer";
            var oppRobotHealth = Robots[oppId].Health;

            _service.Equip(oppId, shield);
            _service.Equip(userId, weapon);

            _service.Shoot(userId, oppId);

            Assert.AreNotEqual(oppRobotHealth, Robots[oppId].Health);
        }

        [Test]
        public void ShouldDecreaseHealthWhenShootAnotherRobot()
        {
            var userId = "1";
            var oppId = "2";
            var weapon = "Blaster";
            var oppRobotHealth = Robots[oppId].Health;

            _service.Equip(userId, weapon);
            _service.Shoot(userId, oppId);

            Assert.AreNotEqual(oppRobotHealth, Robots[oppId].Health);
        }

        [Test]
        public void ShouldNotDecreaseHealthIfRightShildIsSetted()
        {
            var userId = "1";
            var oppId = "2";
            var weapon = "Blaster";
            var shield = "ForceShield";
            var oppRobotHealth = Robots[oppId].Health;

            _service.Equip(oppId, shield);
            _service.Equip(userId, weapon);

            _service.Shoot(userId, oppId);

            Assert.AreEqual(oppRobotHealth, Robots[oppId].Health);
        }

        [Test]
        public void ShouldReturnFalseWhenRemoveDoesNotExistingRobot()
        {
            var userId = "-1";

            var isRemoved = _service.RemoveRobot(userId);

            Assert.IsFalse(isRemoved);
        }

        [Test]
        public void ShouldReturnInstanceIfRobotDoesNotExistsWhileCreatingNewInstance()
        {
            var id = "4";

            var robot = _service.CreateRobot(id);

            Assert.IsNotNull(robot);
        }

        [Test]
        public void ShouldReturnRobot()
        {
            var userId = "1";

            var robot = _service.GetRobot(userId);

            Assert.NotNull(robot);
        }

        [Test]
        public void ShouldReturnTrueWhenRemoveRobotByUserId()
        {
            var userId = "1";

            var isRemoved = _service.RemoveRobot(userId);

            Assert.IsTrue(isRemoved);
        }

        [Test]
        public void ShouldThrowExceptionIfRobotAlreadyExistsWhileCreatingNewInstance()
        {
            var id = "1";

            var ex = Assert.Throws<Exception>(() => _service.CreateRobot(id));

            Assert.That(ex.Message, Is.EqualTo($"Robot {id} already exists"));
        }

        [Test]
        public void ShouldThrowExceptionWhenRobotDoesNotExists()
        {
            var userId = "-1";

            var ex = Assert.Throws<NullReferenceException>(() => _service.GetRobot(userId));

            Assert.That(ex.Message, Is.EqualTo($"Robot {userId} does not exists"));
        }

        [Test]
        public void ComplexBattleTest()
        {
            var userId = "10";
            var oppId = "20";
            _service.CreateRobot(userId);
            _service.CreateRobot(oppId);
            _service.Equip(userId, "MachineGun");
            _service.Equip(oppId, "MachineGun");
            _service.Shoot(userId, oppId);
            _service.Shoot(oppId, userId);
            _service.CheckWinner(userId, oppId);
            _service.CheckWinner(oppId, userId);

            _service.Equip(oppId, "MachineGun");
            _service.Equip(userId, "MachineGun");
            _service.Shoot(oppId, userId);
            _service.Shoot(userId, oppId);
            _service.CheckWinner(oppId, userId);
            _service.CheckWinner(userId, oppId);

            _service.Equip(userId, "MachineGun");
            _service.Equip(oppId, "MachineGun");
            _service.Shoot(userId, oppId);
            _service.Shoot(oppId, userId);
            _service.CheckWinner(userId, oppId);
            _service.CheckWinner(oppId, userId);

            _service.Equip(oppId, "MachineGun");
            _service.Equip(userId, "MachineGun");
            _service.Shoot(oppId, userId);
            _service.Shoot(userId, oppId);
            _service.CheckWinner(oppId, userId);
            _service.CheckWinner(userId, oppId);

            _service.Equip(oppId, "MachineGun");
            _service.Equip(userId, "MachineGun");
            _service.Shoot(oppId, userId);
            _service.Shoot(userId, oppId);
            _service.CheckWinner(oppId, userId);
            _service.CheckWinner(userId, oppId);

            _service.Equip(userId, "MachineGun");
            _service.Equip(oppId, "MachineGun");
            _service.Shoot(userId, oppId);
            _service.Shoot(oppId, userId);
            _service.CheckWinner(userId, oppId);
            _service.CheckWinner(oppId, userId);

            _service.Equip(oppId, "MachineGun");
            _service.Equip(userId, "MachineGun");
            _service.Shoot(oppId, userId);
            _service.Shoot(userId, oppId);
            _service.CheckWinner(oppId, userId);
            _service.CheckWinner(userId, oppId);

            _service.Equip(oppId, "MachineGun");
            _service.Equip(userId, "MachineGun");
            _service.Shoot(oppId, userId);
            _service.Shoot(userId, oppId);
            _service.CheckWinner(oppId, userId);
            _service.CheckWinner(userId, oppId);

            _service.Equip(userId, "MachineGun");
            _service.Equip(oppId, "MachineGun");
            _service.Shoot(userId, oppId);
            _service.Shoot(oppId, userId);
            _service.CheckWinner(userId, oppId);
            _service.CheckWinner(oppId, userId);

            _service.Equip(oppId, "MachineGun");
            _service.Equip(userId, "MachineGun");
            _service.Shoot(oppId, userId);
            _service.Shoot(userId, oppId);
            _service.CheckWinner(oppId, userId);
            _service.CheckWinner(userId, oppId);

            _service.Equip(userId, "MachineGun");
            _service.Equip(oppId, "MachineGun");
            _service.Shoot(userId, oppId);
            _service.Shoot(oppId, userId);
            _service.CheckWinner(userId, oppId);
            _service.CheckWinner(oppId, userId);
            var expHealthUser = _service.GetRobot(userId).Health;
            var expHealthOppUser = _service.GetRobot(oppId).Health;

            _service.Equip(oppId, "Blaster");
            _service.Equip(userId, "ForceShield");
            _service.Shoot(oppId, userId);
            _service.Shoot(userId, oppId);
            _service.CheckWinner(oppId, userId);
            _service.CheckWinner(userId, oppId);
            var healthOppUser = _service.GetRobot(oppId).Health;
            var healthUser = _service.GetRobot(userId).Health;
            Assert.IsTrue(expHealthUser.Equals(healthUser) && expHealthOppUser.Equals(healthOppUser));
        }
    }
}