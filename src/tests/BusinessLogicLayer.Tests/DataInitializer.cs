﻿using DataAccessLayer.Entities;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace BusinessLogicLayer.Tests
{
    /// <summary>
    ///     Data initializer for unit tests
    /// </summary>
    public class DataInitializer
    {
        /// <summary>
        ///     Dummy users
        /// </summary>
        /// <returns></returns>
        public static List<User> GetAllUsers()
        {
            return new List<User>
            {
                new User
                {
                    Id = "1",
                    UserName = "TName1",
                    Password = "+wWNjOmNjVbFGGTMHZL9oBgMGyjyUbNo+nMFnAM1wCUT3JKFmKg=",
                    Chats = new List<string> {"1", "2"}
                },
                new User
                {
                    Id = "2",
                    UserName = "TName2",
                    Password = "TPass2",
                    Chats = new List<string> {"1", "3"}
                },
                new User
                {
                    Id = "3",
                    UserName = "TName3",
                    Password = "Tpass3",
                    Chats = new List<string> {"2", "3"}
                },
                new User
                {
                    Id = "4",
                    UserName = "TName4",
                    Password = "TPass4",
                    Chats = new List<string>()
                }
            };
        }

        /// <summary>
        ///     Dummy chats
        /// </summary>
        /// <returns></returns>
        public static List<Chat> GetAllChats()
        {
            return new List<Chat>
            {
                new Chat
                {
                    Id = "1",
                    Messages = new List<Message> {
                        new Message
                        {
                            Id = "1",
                            UserId = "1",
                            Text = "TextMsg1"
                        },
                        new Message
                        {
                            Id = "2",
                            UserId = "2",
                            Text = "TextMsg2"
                        }
                    },
                    Users = new List<string> {"1", "2"}
                },
                new Chat
                {
                    Id = "2",
                    Messages = new List<Message>
                    {
                        new Message
                        {
                            Id = "3",
                            UserId = "1",
                            Text = "TextMsg3"
                        },
                        new Message
                        {
                            Id = "4",
                            UserId = "3",
                            Text = "TextMsg4"
                        }
                    },
                    Users = new List<string> {"1", "3"}
                },
                new Chat
                {
                    Id = "3",
                    Messages = new List<Message>(),
                    Users = new List<string> {"2", "3"}
                }
            };
        }

        /// <summary>
        ///     Dummy robots
        /// </summary>
        /// <returns></returns>
        public static ConcurrentDictionary<string, Robot> GetAllRobots()
        {
            var robots = new ConcurrentDictionary<string, Robot>();
            robots.TryAdd("1", new Robot());
            robots.TryAdd("2", new Robot());
            robots.TryAdd("3", new Robot());
            return robots;
        }
    }
}