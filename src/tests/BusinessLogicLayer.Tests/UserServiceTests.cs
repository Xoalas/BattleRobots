﻿using BusinessLogicLayer.Dto;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using NUnit.Framework;
using System;
using System.Linq;

namespace BusinessLogicLayer.Tests
{
    [TestFixture]
    public class UserServiceTests : BaseServices
    {
        [SetUp]
        public void ChatServiceInitialize()
        {
            _service = new UserService(Uow.Object, Mapper);
        }

        private IUserService _service;

        [TestCase("TName1", "TPass1")]
        [TestCase("TName", "TPass")]
        public void ShouldThrowExceptionWhenTryToAuthenticateWithWrongCredentials(string userName, string password)
        {
            var ex = _service.OAuthIdentity(userName, password).Exception.InnerException;
            Assert.That(ex.Message, Is.EqualTo("The user name or password is incorrect"));
        }

        [Test]
        public void ShouldAuthenticate()
        {
            var userName = "TName1";
            var password = "TPass";

            var claims = _service.OAuthIdentity(userName, password);

            Assert.IsTrue(claims.Result.IsAuthenticated);
        }

        [Test]
        public void ShouldRegisterNewUser()
        {
            var expectedUsersCount = 5;
            var userDto = new UserDto {UserName = "NewTest", Password = "TPass"};

            _service.Register(userDto);

            Assert.AreEqual(expectedUsersCount, Users.Count());
        }

        [Test]
        public void ShouldReturnAllUsers()
        {
            var expectedUsersCount = 4;

            var users = _service.GetUsers();

            Assert.AreEqual(expectedUsersCount, users.Count());
        }

        [Test]
        public void ShouldReturnFalseIfTryRegisterExistingUser()
        {
            var userDto = new UserDto {UserName = "TName1", Password = "NewPass"};

            var isRegistred = _service.Register(userDto);

            Assert.IsFalse(isRegistred);
        }

        [Test]
        public void ShouldReturnFalseWhenUserInformationDoesNotUpdated()
        {
            var user = new UserDto {UserName = "TTName1", Defeats = 1, Victories = 2};

            Assert.IsFalse(_service.UpdateUserInformation(user));
        }

        [Test]
        public void ShouldReturnTrueIfRegisterNewUser()
        {
            var userDto = new UserDto {UserName = "NewTest", Password = "NewPass"};

            var isRegistred = _service.Register(userDto);

            Assert.IsTrue(isRegistred);
        }

        [Test]
        public void ShouldReturnTrueWhenUserInformationUpdatedSuccessfully()
        {
            var user = new UserDto {UserName = "TName1", Defeats = 1, Victories = 2};

            Assert.IsTrue(_service.UpdateUserInformation(user));
        }

        [Test]
        public void ShouldReturnUserById()
        {
            var userId = "1";

            var user = _service.GetUserById(userId);

            Assert.AreEqual(userId, user.Id);
        }

        [Test]
        public void ShouldThrowExceptionWhenUserNotFound()
        {
            var userId = "-1";

            var ex = Assert.Throws<Exception>(() => _service.GetUserById(userId));

            Assert.That(ex.Message, Is.EqualTo($"User {userId} not found"));
        }
    }
}