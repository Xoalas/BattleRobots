﻿using BusinessLogicLayer.Dto;
using System.Collections.Generic;

namespace WebApi.Tests
{
    /// <summary>
    ///     Data initializer for unit tests
    /// </summary>
    public class DataInitializer
    {
        /// <summary>
        ///     Dummy users
        /// </summary>
        /// <returns></returns>
        public static List<UserDto> GetAllUsers()
        {
            return new List<UserDto>
            {
                new UserDto
                {
                    Id = "1",
                    UserName = "TName1",
                    Password = "+wWNjOmNjVbFGGTMHZL9oBgMGyjyUbNo+nMFnAM1wCUT3JKFmKg=",
                },
                new UserDto
                {
                    Id = "2",
                    UserName = "TName2",
                    Password = "TPass2",
                },
                new UserDto
                {
                    Id = "3",
                    UserName = "TName3",
                    Password = "Tpass3",
                },
                new UserDto
                {
                    Id = "4",
                    UserName = "TName4",
                    Password = "TPass4",
                }
            };
        }

        /// <summary>
        ///     Dummy chats
        /// </summary>
        /// <returns></returns>
        public static List<ChatDto> GetAllChats()
        {
            return new List<ChatDto>
            {
                new ChatDto
                {
                    Id = "1",
                },
                new ChatDto
                {
                    Id = "2",
                },
                new ChatDto
                {
                    Id = "3",
                }
            };
        }
    }
}