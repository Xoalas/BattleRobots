﻿using BusinessLogicLayer.Dto;
using BusinessLogicLayer.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http.Results;
using WebApi.Controllers;

namespace WebApi.Tests.Controllers
{
    [TestFixture]
    public class ChatControllerTest
    {
        private ChatController controller;
        private Mock<IChatService> service;

        [SetUp]
        public void Startup()
        {
            var ctx = new Mock<HttpContextBase>();
            var mockIdentity = new Mock<IIdentity>();
            ctx.SetupGet(x => x.User.Identity).Returns(mockIdentity.Object);
            mockIdentity.SetupGet(x => x.Name).Returns("Test1");
            this.service = new Mock<IChatService>();
            controller = new ChatController(service.Object);
        }

        [Test]
        public void GetShouldReturnAllUserChats()
        {
            service.Setup(x => x.GetChatsByUserId(It.IsAny<string>()))
                .Returns<string>(c => DataInitializer.GetAllChats());

            var contentResult = controller.Get() as JsonResult<IEnumerable<ChatDto>>;

            Assert.AreEqual(3, contentResult.Content.Count());
        }

        [Test]
        public void GetChatByIdShouldReturnChat()
        {
            service.Setup(x => x.GetChat(It.IsAny<string>()))
                .Returns<string>(c => DataInitializer.GetAllChats().Find(p => p.Id.Equals(c)));

            var contentResult = controller.Get("1") as JsonResult<ChatDto>;

            Assert.IsNotNull(contentResult.Content);
        }

        [Test]
        public void GetChatByUsersShouldReturnChatBetweenUsers()
        {
            service.Setup(x => x.GetChatByUsers(It.IsAny<string>(), It.IsAny<string>()))
                .Returns<string, string>((user, oppUser) => new ChatDto());

            var contentResult = controller.GetChatByUsers("2") as JsonResult<ChatDto>;

            Assert.IsNotNull(contentResult.Content);
        }
        
        [Test]
        public void SendMessageShouldReturnOk()
        {
            service.Setup(x => x.SendMessage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Callback<string, string, string>((user, chat, text) =>
                {
                    if (!DataInitializer.GetAllUsers().Any(x => x.Id.Equals(user)) && !DataInitializer.GetAllChats().Any(x => x.Id.Equals(chat)))
                        throw new Exception();
                });

            var result = controller.SendMessage("1", "TestText");

            Assert.IsInstanceOf<OkResult>(result);
        }

        [Test]
        public void CreateChatShouldReturnOk()
        {
            service.Setup(x => x.CreateChat(It.IsAny<string>(), It.IsAny<string>()))
                .Returns<string, string>((user, oppUser) =>
                {
                    if (!DataInitializer.GetAllUsers().Any(x => x.Id.Equals(user)) && !DataInitializer.GetAllUsers().Any(x => x.Id.Equals(oppUser)))
                        throw new Exception();
                    return true;
                });

            var result = controller.CreateChat("1");

            Assert.IsInstanceOf<OkResult>(result);
        }
    }
}
