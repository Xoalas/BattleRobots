﻿using AutoMapper;
using BusinessLogicLayer.Dto;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class RobotService : IRobotService
    {
        private static readonly ConcurrentDictionary<string, Tuple<int, string>> Equipment =
            new ConcurrentDictionary<string, Tuple<int, string>>();
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
        private readonly Random _rnd;
        private static readonly object _sync = new object();

        public RobotService(IUnitOfWork db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
            _rnd = new Random(DateTime.Now.Millisecond);

            var section = ConfigurationManager.GetSection("equipmentSettings") as NameValueCollection;

            foreach (string key in section)
            {
                var values = section[key].Split(';');
                Equipment.TryAdd(key, Tuple.Create(int.Parse(values[0]), values[1]));
            }
        }

        public RobotDto CreateRobot(string id)
        {
            if (_db.Robots.Exists(id))
                throw new Exception($"Robot {id} already exists");

            var robot = new Robot();
            _db.Robots.Add(id, robot);
            return _mapper.Map<Robot, RobotDto>(robot);
        }

        public RobotDto GetRobot(string id)
        {
            if (_db.Robots.Exists(id))
                return _mapper.Map<Robot, RobotDto>(_db.Robots.Get(id));
            throw new NullReferenceException($"Robot {id} does not exists");
        }

        public bool RemoveRobot(string id)
        {
            return _db.Robots.Exists(id) && _db.Robots.Remove(id);
        }

        public bool Equip(string id, string value)
        {
            lock (_sync)
            {
                var robot = _db.Robots.Get(id);
                if (robot == null)
                    return false;

                if (Equipment.ContainsKey(value))
                {
                    robot.Weapon = value;
                    robot.Shield = null;
                }
                else if (Equipment.Any(x => x.Value.Item2.Contains(value)))
                {
                    robot.Shield = value;
                    robot.Weapon = null;
                }
                else
                    return false;

                return _db.Robots.Update(id, robot);
            }
        }

        public IEnumerable<string> Shoot(string userId, string oppId)
        {
            lock (_sync)
            {
                var userRobot = _db.Robots.Get(userId);
                var oppRobot = _db.Robots.Get(oppId);
                if (userRobot == null || oppRobot == null)
                    throw new NullReferenceException($"Robot {userId} or {oppId} does not exists");

                if (userRobot.Weapon != null && Equipment.ContainsKey(userRobot.Weapon))
                {
                    var damage = GetDamage(oppRobot, userRobot.Weapon);
                    oppRobot.Health -= damage;
                    _db.Robots.Update(oppId, oppRobot);
                    return new List<string>()
                    {
                        oppId,
                        $"was damaged by {userRobot.Weapon} on {damage} points. Left {oppRobot.Health}/100 healthpoints"
                    };
                }

                if (userRobot.Shield != null && Equipment.Any(x => x.Value.Item2.Contains(userRobot.Shield)))
                    return new List<string>() { userId, $"used {userRobot.Shield}" };

                throw new Exception($"Robot {userId} does not have any equipment");
            }
        }

        public string CheckWinner(string id, string oppId)
        {
            lock (_sync)
            {
                var userRobot = _db.Robots.Get(id);
                var oppRobot = _db.Robots.Get(oppId);

                if (userRobot.Health > 0) return string.Empty;

                if (userRobot.Health <= 0 && oppRobot.Health <= 0) return "Draw";

                SetStats(oppId, id);
                return oppId;
            }
        }

        public void SetStats(string winnerId, string loserId)
        {
            lock (_sync)
            {
                var winner = _db.Users.Get(winnerId);
                var loser = _db.Users.Get(loserId);

                winner.Victories += 1;
                loser.Defeats += 1;

                _db.Users.Update(winner);
                _db.Users.Update(loser);
            }
        }

        private int GetDamage(Robot robot, string weapon)
        {
            lock (_sync)
            {
                if (robot.Shield != null && Equipment[weapon].Item2.Equals(robot.Shield))
                    return 0;
                return _rnd.Next(Equipment[weapon].Item1);
            }
        }
    }
}