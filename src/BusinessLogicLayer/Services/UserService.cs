﻿using AutoMapper;
using BusinessLogicLayer.Dto;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public UserDto GetUserById(string userId)
        {
            var user = _db.Users.Get(userId);
            if (user == null)
                throw new Exception($"User {userId} not found");

            return _mapper.Map<User, UserDto>(user);
        }

        public UserDto GetUserByUserName(string userName)
        {
            var user = _db.Users.Get(u => u.UserName.Equals(userName));
            if (user == null)
                throw new Exception($"User {userName} not found");

            return _mapper.Map<User, UserDto>(user);
        }

        public IEnumerable<UserDto> GetUsers()
        {
            return _mapper.Map<IEnumerable<User>, List<UserDto>>(_db.Users.GetAll());
        }

        public bool Register(UserDto userDto)
        {
            var user = _db.Users.Get(u => u.UserName.Equals(userDto.UserName));

            if (user != null || !Regex.IsMatch(userDto.Password, "^([a-zA-Z0-9@*#_]{8,15})$")) return false;

            user = new User
            {
                UserName = userDto.UserName,
                Password = Encrypt(userDto.Password, null)
            };

            _db.Users.Create(user);
            return true;
        }

        public bool UpdateUserInformation(UserDto userDto)
        {
            var user = _db.Users.Get(u => u.UserName.Equals(userDto.UserName));
            if (user == null) return false;
            user.Victories = userDto.Victories;
            user.Defeats = userDto.Defeats;
            _db.Users.Update(user);
            return true;
        }

        public async Task<ClaimsIdentity> OAuthIdentity(string userName, string password)
        {
            var user = _db.Users.Get(u => u.UserName == userName && Verify(password, u.Password));
            if (user == null)
                throw new Exception("The user name or password is incorrect");

            return await CreateAsync(user, OAuthDefaults.AuthenticationType);
        }

        public async Task<ClaimsIdentity> CookiesIdentity(string userName, string password)
        {
            var user = _db.Users.Get(u => u.UserName == userName && Verify(password, u.Password));
            if (user == null)
                throw new Exception("The user name or password is incorrect");

            return await CreateAsync(user, CookieAuthenticationDefaults.AuthenticationType);
        }

        private async Task<ClaimsIdentity> CreateAsync(User user, string authenticationType)
        {
            var claimsIdentity = new ClaimsIdentity(authenticationType, ClaimTypes.NameIdentifier, ClaimTypes.Role);
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id,
                "http://www.w3.org/2001/XMLSchema#string"));
            claimsIdentity.AddClaim(
                new Claim(ClaimTypes.Name, user.UserName, "http://www.w3.org/2001/XMLSchema#string"));
            claimsIdentity.AddClaim(new Claim(
                "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "Custom Identity",
                "http://www.w3.org/2001/XMLSchema#string"));
            return claimsIdentity;
        }

        #region hash

        private static string Encrypt(string plaintText, byte[] saltBytes)
        {
            if (saltBytes == null)
            {
                var rnd = new Random();
                saltBytes = new byte[rnd.Next(4, 8)];
                var rng = new RNGCryptoServiceProvider();
                rng.GetNonZeroBytes(saltBytes);
            }

            var plaintTextBytes = Encoding.UTF8.GetBytes(plaintText);
            var plainTextWithSaltBytes = new byte[plaintTextBytes.Length + saltBytes.Length];
            for (var i = 0; i < plaintTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plaintTextBytes[i];

            for (var i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plaintTextBytes.Length + i] = saltBytes[i];

            var hash = new SHA256Managed();
            var hashBytes = hash.ComputeHash(plainTextWithSaltBytes);
            var hashWithSaltBytes = new byte[hashBytes.Length + saltBytes.Length];
            for (var i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            for (var i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            for (var i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            return Convert.ToBase64String(hashWithSaltBytes);
        }

        private static bool Verify(string plaintText, string hashValue)
        {
            var hashWithSaltBytes = Convert.FromBase64String(hashValue);
            const int hashSizeInBits = 256;
            const int hashSizeInBytes = hashSizeInBits / 8;

            if (hashWithSaltBytes.Length < hashSizeInBytes)
                return false;

            var saltBytes = new byte[hashWithSaltBytes.Length - hashSizeInBytes];

            for (var i = 0; i < saltBytes.Length; i++)
                saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

            return hashValue == Encrypt(plaintText, saltBytes);
        }

        #endregion
    }
}