﻿using AutoMapper;
using BusinessLogicLayer.Dto;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class ChatService : IChatService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
        private static readonly object _sync = new object();

        public ChatService(IUnitOfWork db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public bool CreateChat(string userId, string oppId)
        {
            lock (_sync)
            {
                var user = _db.Users.Get(userId);
                if (user == null)
                    throw new Exception($"User {userId} not found");

                var opp = _db.Users.Get(oppId);
                if (opp == null)
                    throw new Exception($"User {oppId} not found");

                if (user.Chats.Intersect(opp.Chats).Any()) return false;
                var chat = new Chat();
                chat.Users.Add(userId);
                chat.Users.Add(oppId);
                _db.Chats.Create(chat);

                user.Chats.Add(chat.Id);
                _db.Users.Update(user);

                opp.Chats.Add(chat.Id);
                _db.Users.Update(opp);
                return true;
            }
        }

        public ChatDto GetChat(string chatId)
        {
            var chat = _db.Chats.Get(chatId);
            if (chat == null)
                throw new Exception($"Chat {chatId} not found");

            return _mapper.Map<Chat, ChatDto>(chat);
        }

        public IEnumerable<ChatDto> GetChatsByUserId(string userId)
        {
            var chats = _db.Chats.GetAll(x => x.Users.Contains(userId));
            return _mapper.Map<IEnumerable<Chat>, List<ChatDto>>(chats);
        }

        public ChatDto GetChatByUsers(string userId, string oppId)
        {
            lock (_sync)
            {
                while (true)
                {
                    var user = _db.Users.Get(userId);
                    if (user == null) throw new Exception($"User {userId} not found");

                    var opp = _db.Users.Get(oppId);
                    if (opp == null) throw new Exception($"User {oppId} not found");

                    var chatId = user.Chats.Where(item => opp.Chats.Any(c => c.Equals(item))).FirstOrDefault();

                    if (chatId != null) return GetChat(chatId);
                    CreateChat(userId, oppId);
                }
            }
        }

        public IEnumerable<MessageDto> GetMessagesByChatId(string chatId)
        {
            var messages = _db.Chats.Get(chatId).Messages;
            return _mapper.Map<IEnumerable<Message>, List<MessageDto>>(messages);
        }

        public void SendMessage(string userId, string chatId, string text)
        {
            var chat = _db.Chats.Get(chatId);
            var user = _db.Users.Get(userId);
            if (user == null)
                throw new Exception($"User {userId} not found");

            var msg = new Message
            {
                Text = text,
                UserId = userId
            };
            chat.Messages.Add(msg);
            _db.Chats.Update(chat);
        }

        public IEnumerable<UserDto> GetChatMembersByChatId(string chatId)
        {
            var chat = _db.Chats.Get(chatId);
            if (chat == null)
                throw new Exception("Chat not found");

            return _mapper.Map<IEnumerable<User>, List<UserDto>>(_db.Users.GetAll()
                .Where(c => c.Chats.Contains(chatId)));
        }
    }
}