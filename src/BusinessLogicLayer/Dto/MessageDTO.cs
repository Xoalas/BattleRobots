﻿namespace BusinessLogicLayer.Dto
{
    public class MessageDto
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string UserId { get; set; }
    }
}