﻿namespace BusinessLogicLayer.Dto
{
    public class RobotDto
    {
        public int Health { get; set; }
        public string Weapon { get; set; }
        public string Shield { get; set; }
    }
}