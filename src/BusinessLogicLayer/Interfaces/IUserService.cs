﻿using BusinessLogicLayer.Dto;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IUserService
    {
        UserDto GetUserById(string userId);
        UserDto GetUserByUserName(string userName);
        IEnumerable<UserDto> GetUsers();
        bool Register(UserDto userDto);
        bool UpdateUserInformation(UserDto userDto);
        Task<ClaimsIdentity> OAuthIdentity(string userName, string password);
        Task<ClaimsIdentity> CookiesIdentity(string userName, string password);
    }
}