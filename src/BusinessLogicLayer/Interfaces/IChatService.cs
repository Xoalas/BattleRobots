﻿using BusinessLogicLayer.Dto;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface IChatService
    {
        IEnumerable<MessageDto> GetMessagesByChatId(string chatId);
        void SendMessage(string userId, string chatId, string text);
        bool CreateChat(string userId, string oppId);
        ChatDto GetChat(string chatId);
        IEnumerable<ChatDto> GetChatsByUserId(string userId);
        ChatDto GetChatByUsers(string userId, string oppId);
        IEnumerable<UserDto> GetChatMembersByChatId(string chatId);
    }
}