﻿using System.Collections.Generic;
using BusinessLogicLayer.Dto;

namespace BusinessLogicLayer.Interfaces
{
    public interface IRobotService
    {
        RobotDto CreateRobot(string id);
        bool RemoveRobot(string id);
        RobotDto GetRobot(string id);
        bool Equip(string id, string equipment);
        IEnumerable<string> Shoot(string userId, string oppId);
        void SetStats(string winnerId, string loserId);
        string CheckWinner(string userId, string oppId);
    }
}