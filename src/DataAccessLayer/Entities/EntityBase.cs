﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace DataAccessLayer.Entities
{
    public abstract class EntityBase
    {
        protected EntityBase()
        {
            Id = Guid.NewGuid().ToString();
        }

        [BsonId] public string Id { get; set; }
    }
}