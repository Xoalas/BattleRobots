﻿using System.Collections.Generic;

namespace DataAccessLayer.Entities
{
    public class User : EntityBase
    {
        public User()
        {
            Chats = new List<string>();
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public int Victories { get; set; }
        public int Defeats { get; set; }
        public virtual ICollection<string> Chats { get; set; }
    }
}