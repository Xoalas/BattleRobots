﻿namespace DataAccessLayer.Entities
{
    public class Robot
    {
        public Robot()
        {
            Health = 100;
        }

        public int Health { get; set; }
        public string Weapon { get; set; }
        public string Shield { get; set; }
    }
}