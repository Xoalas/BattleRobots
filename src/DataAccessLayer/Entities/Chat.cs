﻿using System.Collections.Generic;

namespace DataAccessLayer.Entities
{
    public class Chat : EntityBase
    {
        public Chat()
        {
            Users = new List<string>();
            Messages = new List<Message>();
        }

        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<string> Users { get; set; }
    }
}