﻿namespace DataAccessLayer.Entities
{
    public class Message : EntityBase
    {
        public string Text { get; set; }
        public string UserId { get; set; }
    }
}