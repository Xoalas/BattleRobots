﻿using System;
using System.Collections.Concurrent;

namespace DataAccessLayer.Repositories
{
    internal static class CacheStore
    {
        /// <summary>
        ///     In-memory cache dictionary
        /// </summary>
        private static readonly ConcurrentDictionary<string, object> Cache = new ConcurrentDictionary<string, object>();

        private static readonly object Sync = new object();

        /// <summary>
        ///     Check if an object exists in cache
        /// </summary>
        /// <param name="key">Name of key in cache</param>
        /// <returns>True, if yes; False, otherwise</returns>
        public static bool Exists(string key)
        {
            lock (Sync)
            {
                return Cache.ContainsKey(key);
            }
        }

        /// <summary>
        ///     Get an object from cache
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="key">Name of key in cache</param>
        /// <returns>Object from cache</returns>
        public static T Get<T>(string key) where T : class
        {
            lock (Sync)
            {
                if (Cache.ContainsKey(key) == false)
                    throw new ApplicationException($"An object with key '{key}' does not exists");
                lock (Sync)
                {
                    return (T) Cache[key];
                }
            }
        }

        /// <summary>
        ///     Add an object to cache
        /// </summary>
        /// <param name="key">Key of the object</param>
        /// <param name="value">Object value</param>
        /// <typeparam name="T">Type of object</typeparam>
        public static bool Add<T>(string key, T value)
        {
            var type = typeof(T);
            if (value.GetType() != type)
                throw new ApplicationException(
                    $"The type of value passed to cache {value.GetType().FullName} does not match the cache type {type.FullName} for key {key}");
            lock (Sync)
            {
                if (Cache.ContainsKey(key))
                    throw new ApplicationException($"An object with key '{key}' already exists");
                lock (Sync)
                {
                    return Cache.TryAdd(key, value);
                }
            }
        }

        /// <summary>
        ///     Add an object to cache
        /// </summary>
        /// <param name="key">Key of the object</param>
        /// <param name="value">Object value</param>
        /// <typeparam name="T">Type of object</typeparam>
        public static bool Update<T>(string key, T value)
        {
            var type = typeof(T);
            if (value.GetType() != type)
                throw new ApplicationException(
                    $"The type of value passed to cache {value.GetType().FullName} does not match the cache type {type.FullName} for key {key}");
            lock (Sync)
            {
                if (!Cache.ContainsKey(key))
                    throw new ApplicationException($"An object with key '{key}' does not exists");
                lock (Sync)
                {
                    return Cache.TryUpdate(key, value, Cache[key]);
                }
            }
        }

        /// <summary>
        ///     Remove an object stored with a key from cache
        /// </summary>
        /// <param name="key">Key of the object</param>
        public static bool Remove(string key)
        {
            lock (Sync)
            {
                if (Cache.ContainsKey(key) == false)
                    throw new ApplicationException($"An object with key '{key}' does not exists in cache");
                lock (Sync)
                {
                    return Cache.TryRemove(key, out var value);
                }
            }
        }
    }
}