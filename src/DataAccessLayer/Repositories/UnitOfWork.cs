﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private static readonly object Sync = new object();
        private IRepository<Chat> _chatRepository;
        private readonly ApplicationContext _db;
        private IRepository<User> _profileRepository;
        private ICacheRepository<Robot> _robotRepository;

        public UnitOfWork(string connectionString)
        {
            _db = new ApplicationContext(connectionString);
        }

        public IRepository<Chat> Chats
        {
            get
            {
                if (_chatRepository == null)
                    lock (Sync)
                    {
                        if (_chatRepository == null)
                            _chatRepository = new GenericRepository<Chat>(_db);
                    }

                return _chatRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (_profileRepository == null)
                    lock (Sync)
                    {
                        if (_profileRepository == null)
                            _profileRepository = new GenericRepository<User>(_db);
                    }

                return _profileRepository;
            }
        }

        public ICacheRepository<Robot> Robots
        {
            get
            {
                if (_robotRepository == null)
                    lock (Sync)
                    {
                        if (_robotRepository == null)
                            _robotRepository = new CacheRepository<Robot>();
                    }

                return _robotRepository;
            }
        }
    }
}