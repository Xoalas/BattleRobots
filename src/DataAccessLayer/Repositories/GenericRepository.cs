﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private readonly IMongoCollection<TEntity> _collection;

        public GenericRepository(ApplicationContext context)
        {
            _collection = context.Database.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public void Create(TEntity entity)
        {
            _collection.InsertOne(entity);
        }

        public void Delete(string id)
        {
            _collection.DeleteOne(c => c.Id == id);
        }

        public void Delete(Func<TEntity, bool> predicate)
        {
            var entities = _collection.AsQueryable().Where(predicate).AsQueryable();
            foreach (var entity in entities)
                _collection.DeleteOne(entity.Id);
        }

        public TEntity Get(Func<TEntity, bool> predicate)
        {
            return _collection.AsQueryable().Where(predicate).FirstOrDefault();
        }

        public TEntity Get(string id)
        {
            return _collection.Find(c => c.Id == id).FirstOrDefault();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _collection.Find(c => true).ToList();
        }

        public void Update(TEntity entity)
        {
            _collection.ReplaceOne(m => m.Id == entity.Id, entity);
        }

        public IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate)
        {
            return _collection.AsQueryable().Where(predicate).ToList();
        }
    }
}