﻿using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Repositories
{
    public class CacheRepository<T> : ICacheRepository<T> where T : class
    {
        public bool Add(string key, T value)
        {
            return CacheStore.Add(key, value);
        }

        public bool Exists(string key)
        {
            return CacheStore.Exists(key);
        }

        public T Get(string key)
        {
            return CacheStore.Get<T>(key);
        }

        public bool Remove(string key)
        {
            return CacheStore.Remove(key);
        }

        public bool Update(string key, T value)
        {
            return CacheStore.Update(key, value);
        }
    }
}