﻿using MongoDB.Driver;

namespace DataAccessLayer.Context
{
    public class ApplicationContext
    {
        public ApplicationContext(string connectionString)
        {
            var connection = new MongoUrlBuilder(connectionString);
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase(connection.DatabaseName);
            Database = database;
        }

        public IMongoDatabase Database { get; }
    }
}