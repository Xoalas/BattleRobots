﻿using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Chat> Chats { get; }
        IRepository<User> Users { get; }
        ICacheRepository<Robot> Robots { get; }
    }
}