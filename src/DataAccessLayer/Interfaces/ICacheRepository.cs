﻿namespace DataAccessLayer.Interfaces
{
    public interface ICacheRepository<T> where T : class
    {
        bool Exists(string key);
        T Get(string key);
        bool Add(string key, T value);
        bool Remove(string key);
        bool Update(string key, T value);
    }
}