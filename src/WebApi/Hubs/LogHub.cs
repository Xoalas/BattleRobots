﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebApi.Hubs
{
    [HubName("LogHub")]
    public class LogHub : Hub
    {
        private static readonly ConcurrentDictionary<string, List<string>> Users =
            new ConcurrentDictionary<string, List<string>>();

        private List<string> GetSubscribers(string senderId, string receiverId)
        {
            var subscribers = new List<string>();
            if (Users[senderId].Count > 0)
                subscribers.AddRange(Users[senderId]);
            if (Users[receiverId].Count > 0)
                subscribers.AddRange(Users[receiverId]);

            return subscribers;
        }

        public void SendLog(string userId, string senderId, string receiverId, string message)
        {
            if (!Users.ContainsKey(senderId))
                Users.TryAdd(senderId, new List<string>());
            if (!Users.ContainsKey(receiverId))
                Users.TryAdd(receiverId, new List<string>());

            Clients.User(senderId).addLog(userId, message);
            Clients.User(receiverId).addLog(userId, message);

            var subscribers = GetSubscribers(senderId, receiverId);

            if (subscribers.Count > 0)
                Clients.Users(subscribers).addLog(userId, message);
        }

        public void SendNotification(string senderId, string receiverId, string message)
        {
            var subscribers = GetSubscribers(senderId, receiverId);

            if (subscribers.Count > 0)
                Clients.Users(subscribers).notify(message);
        }

        public void Subscribe(string id, string userId)
        {
            var key = Users.FirstOrDefault(x => x.Value.Contains(id)).Key;
            if (key != null)
                Users[key].Remove(id);
            if (Users.ContainsKey(userId))
                Users[userId].Add(id);
            else
                Users.TryAdd(userId, new List<string> {id});
        }

        public void Unsubscribe(string id)
        {
            var key = Users.FirstOrDefault(x => x.Value.Contains(id)).Key;
            if (key != null)
                Users[key].Remove(id);
        }
    }
}