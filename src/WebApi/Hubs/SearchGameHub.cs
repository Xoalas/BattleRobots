﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebApi.Hubs
{
    [HubName("SearchGameHub")]
    public class SearchGameHub : Hub
    {
        private static readonly ConcurrentDictionary<string, string> ConnectedUsers =
            new ConcurrentDictionary<string, string>();

        private readonly object _sync = new object();
        private string _userId = "";

        public void SearchGame(string id)
        {
            lock (_sync)
            {
                var user = ConnectedUsers.FirstOrDefault(x => x.Key != id).Key;
                if (user != null)
                    Clients.Caller.getPlayer(user);
                else
                    Clients.Caller.callback();
            }
        }

        public void DoConnect()
        {
            _userId = Context.QueryString["UserId"];
            ConnectedUsers.TryRemove(_userId, out var oldId);
            ConnectedUsers.TryAdd(_userId, Context.ConnectionId);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            if (stopCalled)
            {
                var id = Context.ConnectionId;
                _userId = Context.QueryString["UserId"];
                ConnectedUsers.TryRemove(_userId, out id);
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}