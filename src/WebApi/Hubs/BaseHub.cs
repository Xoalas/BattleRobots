﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace WebApi.Hubs
{
    public abstract class BaseHub : Hub
    {
        public override Task OnConnected()
        {
            DoConnect();
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            DoConnect();
            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            if (stopCalled)
                DoDisconnect();
            return base.OnDisconnected(stopCalled);
        }

        protected abstract void DoConnect();
        protected abstract void DoDisconnect();
    }
}