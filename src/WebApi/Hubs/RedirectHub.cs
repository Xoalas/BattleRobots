﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebApi.Hubs
{
    [HubName("RedirectHub")]
    public class RedirectHub : Hub
    {
        public void SendRequest(string currentUserId, string userId, string userName)
        {
            Clients.User(userId).request(currentUserId, userName);
        }

        public void SendCallBack(string currentUserId, string userId, string userName, bool isAgree)
        {
            Clients.User(userId).callback(currentUserId, userName, isAgree);
        }
    }
}