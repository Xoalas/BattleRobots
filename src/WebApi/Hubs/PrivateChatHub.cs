﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebApi.Hubs
{
    [HubName("PrivateChatHub")]
    public class PrivateChatHub : Hub
    {
        public void SendChatMessage(string sender, string receiver, string message)
        {
            Clients.User(sender).addMessage(sender, message);
            Clients.User(receiver).addMessage(sender, message);
        }
    }
}