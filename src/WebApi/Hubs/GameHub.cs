﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebApi.Hubs
{
    [HubName("GameHub")]
    public class GameHub : Hub
    {
        public void SendRequest(string userId)
        {
            Clients.User(userId).request();
        }

        public void SendCallback(string userId)
        {
            Clients.User(userId).callback();
        }

        public void SendAlert(string userId, string message)
        {
            Clients.Caller.alert(message);
            Clients.User(userId).alert(message);
        }

        public void Disconnect(string userId)
        {
            Clients.User(userId).stop();
        }
    }
}