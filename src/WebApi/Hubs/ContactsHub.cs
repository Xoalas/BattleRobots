﻿using System.Collections.Concurrent;
using System.Linq;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace WebApi.Hubs
{
    [HubName("ContactsHub")]
    public class ContactsHub : BaseHub
    {
        private static readonly ConcurrentDictionary<string, string> ConnectedUsers =
            new ConcurrentDictionary<string, string>();

        private string _userName = "";

        public void GetContacts()
        {
            var users = JsonConvert.SerializeObject(ConnectedUsers.Select(u => u.Key));
            Clients.All.addContacts(users);
        }

        protected override void DoConnect()
        {
            _userName = Context.QueryString["User-Name"];
            ConnectedUsers.TryRemove(_userName, out var oldId);
            ConnectedUsers.TryAdd(_userName, Context.ConnectionId);
        }

        protected override void DoDisconnect()
        {
            _userName = Context.QueryString["User-Name"];
            ConnectedUsers.TryRemove(_userName, out _);
            var users = JsonConvert.SerializeObject(ConnectedUsers.Select(u => u.Key));
            Clients.All.onUserDisconnected(users);
        }
    }
}