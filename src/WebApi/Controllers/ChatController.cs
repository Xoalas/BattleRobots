﻿using System;
using System.Linq;
using System.Web.Http;
using BusinessLogicLayer.Interfaces;
using Microsoft.AspNet.Identity;

namespace WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/chats")]
    public class ChatController : ApiController
    {
        private readonly IChatService _service;

        /// <summary>Chats Controller constructor</summary>
        public ChatController(IChatService service)
        {
            this._service = service;
        }

        /// <summary>Get chats</summary>
        /// <remarks>Get list of all user chats</remarks>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                return Json(_service.GetChatsByUserId(User.Identity.GetUserId()));
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get chat</summary>
        /// <remarks>Get chat by id</remarks>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            try
            {
                var chat = _service.GetChat(id);
                return Json(chat);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get chat</summary>
        /// <remarks>Get chat with friend by id</remarks>
        /// <returns></returns>
        [Route("user/{id}")]
        [HttpGet]
        public IHttpActionResult GetChatByUsers(string id)
        {
            try
            {
                var chat = _service.GetChatByUsers(User.Identity.GetUserId(), id);
                return Json(chat);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Send message</summary>
        /// <remarks>Send message to chat</remarks>
        /// <returns></returns>
        [Route("{chatId}/send")]
        [HttpPost]
        public IHttpActionResult SendMessage(string chatId, [FromBody] string text)
        {
            try
            {
                _service.SendMessage(User.Identity.GetUserId(), chatId, text);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Create chat</summary>
        /// <remarks>Create chat with friend</remarks>
        /// <returns></returns>
        [Route("create/{id}")]
        [HttpPost]
        public IHttpActionResult CreateChat(string id)
        {
            try
            {
                _service.CreateChat(User.Identity.GetUserId(), id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Open chat</summary>
        /// <remarks>Open chat and get a list of messages from chat</remarks>
        /// <returns></returns>
        [Route("{chatId}/messages")]
        [HttpGet]
        public IHttpActionResult OpenChat(string chatId)
        {
            try
            {
                var messages = _service.GetMessagesByChatId(chatId);
                return Json(messages);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get chat members</summary>
        /// <remarks>Get all chat members</remarks>
        /// <returns></returns>
        [Route("{chatId}/members")]
        [HttpGet]
        public IHttpActionResult GetChatMembers(string chatId)
        {
            try
            {
                var users = _service.GetChatMembersByChatId(chatId);
                if (users.Count() == 0)
                    throw new Exception("Not found users");
                return Json(users);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}