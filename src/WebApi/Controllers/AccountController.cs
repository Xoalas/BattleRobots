﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using BusinessLogicLayer.Dto;
using BusinessLogicLayer.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using WebApi.Models;

namespace WebApi.Controllers
{
    /// <summary>Account Controller</summary>
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private readonly IUserService _userService;

        /// <summary>Account Controller constructor</summary>
        public AccountController(IUserService userService)
        {
            this._userService = userService;
        }

        /// <summary>Token</summary>
        public ISecureDataFormat<AuthenticationTicket> AccessToken { get; private set; }

        private IAuthenticationManager Authentication => Request.GetOwinContext().Authentication;

        /// <summary>Logout</summary>
        /// <remarks>User logout</remarks>
        /// <returns></returns>
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        /// <summary>Register</summary>
        /// <remarks>User registration</remarks>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("Register")]
        public IHttpActionResult Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = new UserDto
            {
                UserName = model.UserName,
                Password = model.Password
            };

            if (!_userService.Register(user))
                return BadRequest();
            return Ok();
        }

        /// <summary>Get current user</summary>
        /// <remarks>Get current authorized user</remarks>
        /// <returns></returns>
        [Route("current")]
        [HttpGet]
        public IHttpActionResult CurrentUser()
        {
            var user = _userService.GetUserById(User.Identity.GetUserId());
            return Json(user);
        }

        /// <summary>Get user</summary>
        /// <remarks>Get user by id</remarks>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            try
            {
                var user = _userService.GetUserById(id);
                return Json(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get user</summary>
        /// <remarks>Get user by userName</remarks>
        /// <returns></returns>
        [Route("name/{userName}")]
        [HttpGet]
        public IHttpActionResult ByUserName(string userName)
        {
            try
            {
                var user = _userService.GetUserByUserName(userName);
                return Json(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get all users</summary>
        /// <remarks>Get a list of all users</remarks>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var users = _userService.GetUsers();
                if (users.ToList().Count == 0)
                    throw new Exception("Users not found");
                return Json(users);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update([FromBody] UserModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var user = new UserDto
                {
                    UserName = model.UserName,
                    Victories = model.Victories,
                    Defeats = model.Defeats
                };

                if (!_userService.UpdateUserInformation(user))
                    return BadRequest();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}