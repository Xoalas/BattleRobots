﻿using System;
using System.Web.Http;
using BusinessLogicLayer.Interfaces;
using Microsoft.AspNet.Identity;

namespace WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/game")]
    public class GameController : ApiController
    {
        private readonly IRobotService _service;

        /// <summary>Robots Controller constructor</summary>
        public GameController(IRobotService service)
        {
            this._service = service;
        }

        /// <summary>Create robot</summary>
        /// <remarks>Create robot by userId</remarks>
        /// <returns></returns>
        [Route("create")]
        [HttpGet]
        public IHttpActionResult CreateRobot()
        {
            try
            {
                var robot = _service.CreateRobot(User.Identity.GetUserId());
                return Json(robot);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get robot by userId</summary>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var robot = _service.GetRobot(User.Identity.GetUserId());
                return Json(robot);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get robot by opponentId</summary>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            try
            {
                var robot = _service.GetRobot(id);
                return Json(robot);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Set winner and loser user stats</summary>
        /// <returns></returns>
        [Route("stats/{winnerId}")]
        [HttpPost]
        public IHttpActionResult Stats(string winnerId, [FromBody] string loserId)
        {
            try
            {
                _service.SetStats(winnerId, loserId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Shoot another robot</summary>
        /// <returns></returns>
        [Route("shoot/{id}")]
        [HttpGet]
        public IHttpActionResult Shoot(string id)
        {
            try
            {
                var result = _service.Shoot(User.Identity.GetUserId(), id);
                return Json(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Get robot by opponentId</summary>
        /// <returns></returns>
        [Route("result/{id}")]
        [HttpGet]
        public IHttpActionResult CheckResults(string id)
        {
            try
            {
                var stat = _service.CheckWinner(User.Identity.GetUserId(), id);
                if (stat is null)
                    throw new Exception("Robot with this id does not exist");
                return Json(stat);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Equip robot</summary>
        /// <returns></returns>
        [Route("equip/{equipment}")]
        [HttpPost]
        public IHttpActionResult Equip(string equipment)
        {
            try
            {
                _service.Equip(User.Identity.GetUserId(), equipment);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        /// <summary>Remove robot by userId</summary>
        /// <returns></returns>
        [Route("remove")]
        [HttpDelete]
        public IHttpActionResult RemoveRobot()
        {
            try
            {
                _service.RemoveRobot(User.Identity.GetUserId());
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}