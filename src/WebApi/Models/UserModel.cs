﻿namespace WebApi.Models
{
    public class UserModel
    {
        public string UserName { get; set; }
        public int Victories { get; set; }
        public int Defeats { get; set; }
    }
}