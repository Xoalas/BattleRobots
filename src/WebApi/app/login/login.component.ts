﻿import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { LoginForm } from "../models/loginForm";

@Component({
    selector: "login",
    styleUrls: [`app/login/login.component.css`],
    template: `
<div class="outer">
    <p id="loginErr"></p>
    <div class="inner">
        <div class="elements">
            <input type="text" class="text" placeholder="Username" [(ngModel)]="form.UserName" />
            <input (keyup.enter)="signIn()" type="password" id="pass" class="text" placeholder="Password" [(ngModel)]="form.Password" />
            <button (click)="signIn()" id="signIn" class="button">SIGN IN</button>
            <button (click)="signUp()" id="signUp" class="button">SIGN UP</button>
        </div>
    </div>
</div>`
})
export class LoginComponent {
    private form = new LoginForm();

    constructor(private authService: AuthService, private router: Router) {}

    private signIn() {
        this.authService.signIn("Token", this.form);
    }

    private signUp() {
        this.router.navigate([{ outlets: { primary: ["register"] } }], { skipLocationChange: true });
    }
}