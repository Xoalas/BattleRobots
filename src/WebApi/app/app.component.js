"use strict";
/// <reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../node_modules/@types/signalr/index.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        document.oncontextmenu = function () { return false; };
        document.onselectstart = function () { return false; };
        if (!localStorage.getItem("access_token"))
            this.router.navigate([{ outlets: { primary: ["login"], navBar: null, contact: null, general: null, bottom: null } }], { skipLocationChange: true });
        else
            this.router.navigate([
                {
                    outlets: {
                        primary: null,
                        navBar: ["nav"],
                        contact: ["contact"],
                        general: ["gameInit"],
                        bottom: ["chat"]
                    }
                }
            ], { skipLocationChange: true });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "my-app",
        styleUrls: ["app/app.component.css?" + new Date().getTime().toString()],
        template: "\n<div class=\"appContainer\">\n    <div class=\"appTop\">\n        <router-outlet name=\"navBar\"></router-outlet>\n    </div>\n    <div class=\"appLeft\">\n        <div class=\"appCenter\">\n            <router-outlet name=\"general\"></router-outlet>\n        </div>\n        <div class=\"appBottom\">\n            <router-outlet name=\"bottom\"></router-outlet>\n        </div>\n    </div>\n    <div class=\"appRight\">\n        <router-outlet name=\"contact\"></router-outlet>\n    </div>    \n</div>\n<router-outlet></router-outlet>"
    }),
    __metadata("design:paramtypes", [router_1.Router])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map