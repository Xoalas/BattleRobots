"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var game_service_1 = require("../services/game.service");
var searchGame_service_1 = require("../services/searchGame.service");
var logGame_service_1 = require("../services/logGame.service");
var GameInitComponent = (function () {
    function GameInitComponent(gameService, searchGameService, logService) {
        this.gameService = gameService;
        this.searchGameService = searchGameService;
        this.logService = logService;
    }
    GameInitComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.init();
        $("#disc").hide();
        $("#searchGame").hide();
        $(document).ready(function () {
            $("#conf").click(function () {
                if (_this.points > 0)
                    alert("You still have " + _this.points + " unallocated points");
                else {
                    $(".centered, .img").attr("disabled", "disabled").off("mousedown");
                    $("#conf").hide();
                    $("#disc, #searchGame").show();
                    _this.searchGameHub.invoke("DoConnect");
                }
            });
            $("#disc").on("click", function () {
                _this.searchGameService.stop();
                _this.init();
                $(".gameInitial *").prop("disabled", false);
                $("#conf").show();
                $("#disc, #searchGame").hide();
                $(".subtractAction").prop("disabled", true);
            });
            $("#searchGame").on("click", function () {
                _this.searchGameHub.invoke("SearchGame", localStorage.getItem("userId"));
            });
        });
    };
    GameInitComponent.prototype.init = function () {
        this.gameService.init();
        this.logService.init();
        this.logService.start();
        this.searchGameService.init();
        this.action = this.gameService.getAction();
        this.points = this.gameService.getPoint();
        this.searchGameHub = this.searchGameService.getHub();
        this.searchGameService.start();
        this.equipLoading();
    };
    GameInitComponent.prototype.equipLoading = function () {
        var _this = this;
        $("#ForceShield").mousedown(function (event) {
            switch (event.which) {
                case 1:
                    if (_this.points > 0 && _this.action.ForceShield !== 3) {
                        _this.action.ForceShield += 1;
                        _this.points -= 1;
                    }
                    break;
                case 3:
                    if (_this.action.ForceShield > 0) {
                        _this.action.ForceShield -= 1;
                        _this.points += 1;
                    }
                    break;
            }
        });
        $("#Maneuver").mousedown(function (event) {
            switch (event.which) {
                case 1:
                    if (_this.points > 0 && _this.action.Maneuver !== 3) {
                        _this.action.Maneuver += 1;
                        _this.points -= 1;
                    }
                    break;
                case 3:
                    if (_this.action.Maneuver > 0) {
                        _this.action.Maneuver -= 1;
                        _this.points += 1;
                    }
                    break;
            }
        });
        $("#Interferer").mousedown(function (event) {
            switch (event.which) {
                case 1:
                    if (_this.points > 0 && _this.action.Interferer !== 2) {
                        _this.action.Interferer += 1;
                        _this.points -= 1;
                    }
                    break;
                case 3:
                    if (_this.action.Interferer > 0) {
                        _this.action.Interferer -= 1;
                        _this.points += 1;
                    }
                    break;
            }
        });
        $("#Blaster").mousedown(function (event) {
            switch (event.which) {
                case 1:
                    if (_this.points > 0 && _this.action.Blaster !== 3) {
                        _this.action.Blaster += 1;
                        _this.points -= 1;
                    }
                    break;
                case 3:
                    if (_this.action.Blaster > 0) {
                        _this.action.Blaster -= 1;
                        _this.points += 1;
                    }
                    break;
            }
        });
        $("#RocketLauncher").mousedown(function (event) {
            switch (event.which) {
                case 1:
                    if (_this.points > 0 && _this.action.RocketLauncher !== 3) {
                        _this.action.RocketLauncher += 1;
                        _this.points -= 1;
                    }
                    break;
                case 3:
                    if (_this.action.RocketLauncher > 0) {
                        _this.action.RocketLauncher -= 1;
                        _this.points += 1;
                    }
                    break;
            }
        });
        $("#Torpedoes").mousedown(function (event) {
            switch (event.which) {
                case 1:
                    if (_this.points > 0 && _this.action.Torpedoes !== 2) {
                        _this.action.Torpedoes += 1;
                        _this.points -= 1;
                    }
                    break;
                case 3:
                    if (_this.action.Torpedoes > 0) {
                        _this.action.Torpedoes -= 1;
                        _this.points += 1;
                    }
                    break;
            }
        });
    };
    GameInitComponent.prototype.ngOnDestroy = function () {
        this.searchGameService.stop();
    };
    return GameInitComponent;
}());
GameInitComponent = __decorate([
    core_1.Component({
        selector: "gameInit",
        styleUrls: ["app/game/game.component.css"],
        template: "\n<div class=\"gameOuter\">\n    <div class=\"gameInitial\">\n        <div class=\"container\">\n            <h1 *ngIf=\"points > 0\">You still have {{points}} unallocated points</h1>\n            <div class=\"row\">\n                <div class=\"rowEl\">\n                    <input type=\"image\" src=\"../../img/ForceShield.png\" alt=\"ForceShield\" class=\"img\">\n                    <label id=\"ForceShield\" class=\"centered\" title=\"Force Shield, protects against Blaster\">{{action.ForceShield}}</label>\n                </div>\n                <div class=\"rowEl\">\n                    <input type=\"image\" src=\"../../img/Blaster.png\" alt=\"Blaster\" class=\"img\">\n                    <label id=\"Blaster\" class=\"centered\" title=\"Blaster, deals 0 to 30 health damage\">{{action.Blaster}}</label>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"rowEl\">\n                    <input type=\"image\" src=\"../../img/Maneuver.png\" alt=\"Maneuver\" class=\"img\">\n                    <label id=\"Maneuver\" class=\"centered\" title=\"Maneuver, protects against Rocket Launcher\">{{action.Maneuver}}</label>\n                </div>\n                <div class=\"rowEl\">\n                    <img src=\"../../img/confirm.png\" alt=\"Confirm\" title=\"Confirm\" id=\"conf\" class=\"btn img\">\n                    <img src=\"../../img/discard.png\" alt=\"Discard\" title=\"Discard\" id=\"disc\" class=\"btn img\">\n                    <img src=\"../../img/search.png\" alt=\"Search Game\" title=\"Search Game\" id=\"searchGame\" class=\"btn img\">\n                </div>\n                <div class=\"rowEl\">\n                    <input type=\"image\" src=\"../../img/RocketLauncher.png\" alt=\"RocketLauncher\" class=\"img\">\n                    <label id=\"RocketLauncher\" class=\"centered\" title=\"Rocket Launcher, deals 0 to 40 health damage\">{{action.RocketLauncher}}</label>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"rowEl\">\n                    <input type=\"image\" src=\"../../img/Interferer.png\" alt=\"Interferer\" class=\"img\">\n                    <label id=\"Interferer\" class=\"centered\" title=\"Interferer, protects against Torpedoes\">{{action.Interferer}}</label>\n                </div>\n                <div class=\"rowEl\">\n                    <input type=\"image\" src=\"../../img/Torpedoes.png\" alt=\"Torpedoes\" class=\"img\">\n                    <label id=\"Torpedoes\" class=\"centered\" title=\"Torpedoes, deals 0 to 50 health damage\">{{action.Torpedoes}}</label>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"
    }),
    __metadata("design:paramtypes", [game_service_1.GameService,
        searchGame_service_1.SearchGameService,
        logGame_service_1.LogGameService])
], GameInitComponent);
exports.GameInitComponent = GameInitComponent;
//# sourceMappingURL=gameInit.component.js.map