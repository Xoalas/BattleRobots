﻿import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { HttpService } from "../services/http.service";
import { LogGameService } from "../services/logGame.service";
import { GameService } from "../services/game.service";
import { Actions } from "../models/actions";
import { Robot } from "../models/robot";
import { User } from "../models/user";
import { configuration } from "../app.constants";
import { Observable, Subscription } from "rxjs/Rx";

@Component({
    selector: "gameAction",
    styleUrls: [`app/game/game.component.css`],
    template: `
<div class="gameOuter">
    <div class="gameAction">
        <div class="actions container">
            <div class="row">
                <div class="rowEl"></div>
                <div class="rowEl">
                    <div *ngIf="action.ForceShield > 0">
                        <input type="image" src="../../img/ForceShield.png" id="ForceShield" (click)="action.ForceShield = action.ForceShield - 1; equip('ForceShield')" alt="ForceShield" class="img">
                        <label for="ForceShield" class="centered" title="Force Shield, protects against Blaster">{{action.ForceShield}}</label>
                    </div>
                </div>
                <div class="rowEl">
                    <div *ngIf="action.Maneuver > 0">
                        <input type="image" src="../../img/Maneuver.png" id="Maneuver" (click)="action.Maneuver = action.Maneuver - 1; equip('Maneuver')" alt="Maneuver" class="img">
                        <label for="Maneuver" class="centered" title="Maneuver, protects against Rocket Launcher">{{action.Maneuver}}</label>
                    </div>
                </div>
                <div class="rowEl">
                    <div *ngIf="action.Interferer > 0">
                        <input type="image" src="../../img/Interferer.png" id="Interferer" (click)="action.Interferer = action.Interferer - 1; equip('Interferer')" alt="Interferer" class="img">
                        <label for="Interferer" class="centered" title="Interferer, protects against Torpedoes">{{action.Interferer}}</label>
                    </div>
                </div>
            </div>
            <div class="row"><div id="tmr">Until the end of the turn left {{ticks}}s</div></div>
            <div class="row">
                <div class="rowEl">
                    <input type="image" src="../../img/MachineGun.png" id="MachineGun" (click)="equip('MachineGun')" alt="MachineGun" class="img">
                    <label for="MachineGun" class="centered" title="Machine Gun, deals 0 to 10 health damage">&#8734;</label>
                </div>
                <div class="rowEl">
                    <div *ngIf="action.Blaster > 0">
                        <input type="image" src="../../img/Blaster.png" id="Blaster" (click)="action.Blaster = action.Blaster - 1; equip('Blaster')" alt="Blaster" class="img">
                        <label for="Blaster" class="centered" title="Blaster, deals 0 to 30 health damage">{{action.Blaster}}</label>
                    </div>
                </div>
                <div class="rowEl">
                    <div *ngIf="action.RocketLauncher > 0">
                        <input type="image" src="../../img/RocketLauncher.png" id="RocketLauncher" (click)="action.RocketLauncher = action.RocketLauncher - 1; equip('RocketLauncher')" alt="RocketLauncher" class="img">
                        <label for="RocketLauncher" class="centered" title="Rocket Launcher, deals 0 to 40 health damage">{{action.RocketLauncher}}</label>
                    </div>
                </div>
                <div class="rowEl">
                    <div *ngIf="action.Torpedoes > 0">
                        <input type="image" src="../../img/Torpedoes.png" id="Torpedoes" (click)="action.Torpedoes = action.Torpedoes - 1; equip('Torpedoes')" alt="Torpedoes" class="img">
                        <label for="Torpedoes" class="centered" title="Torpedoes, deals 0 to 50 health damage">{{action.Torpedoes}}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`,
    providers: [HttpService]
})
export class GameActionComponent implements OnInit, OnDestroy {
    private action: Actions;
    private response: boolean = false;
    private hub: SignalR.Hub.Proxy;
    private connection: SignalR.Hub.Connection;
    private userId = localStorage.getItem("userId");
    private opponentId = localStorage.getItem("opponentUserId");
    private ticks: number = 60;
    private timer: Observable<number>;
    private sub: Subscription;

    constructor(private httpService: HttpService,
        private router: Router,
        private gameService: GameService,
        private logService: LogGameService) {
    }

    ngOnInit() {
        this.logService.unsbuscribe();
        this.action = this.gameService.getAction();
        this.timer = Observable.timer(2000, 1000);
        this.sub = this.timer.subscribe(t => this.tickerFunc());

        this.httpService.get("api/game/create").subscribe();

        this.connection = $.hubConnection(configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("GameHub");

        $(window).on("beforeunload",
            () => {
                this.hub.invoke("Disconnect", localStorage.getItem("opponentUserId"));
                this.stop();
            });

        this.hub.on("request",
            () => {
                if (this.response === false)
                    this.response = true;
            });

        this.hub.on("callback",
            () => {
                this.response = false;
                $(".actions *").prop("disabled", false);
                this.turn();
            });

        this.hub.on("alert",
            (msg) => {
                this.logService.notify(msg);
                this.stop();
                alert(msg);
            });

        this.hub.on("stop",
            () => {
                this.httpService.post(`stats/${localStorage.getItem("opponentUserId")}`,
                    JSON.stringify(localStorage.getItem("userId")));
                alert(localStorage.getItem("userName") + " won!");
                this.stop();
            });

        this.connection.start().done;
    }

    private tickerFunc() {
        if (--this.ticks === 0) {
            this.hub.invoke("Disconnect", localStorage.getItem("opponentUserId"));
            this.stop();
            alert("Time for turn has expired. You lose");
        }
    }

    private equip(equipment: string) {
        $(".actions *").prop("disabled", true);
        this.ticks = 60;
        this.httpService.post(`api/game/equip/${equipment}`);
        if (this.response === false) {
            this.response = true;
            this.hub.invoke("SendRequest", this.opponentId);
        } else {
            $(".actions *").prop("disabled", false);
            this.response = false;
            this.hub.invoke("SendCallback", this.opponentId);
            this.turn();
        }
    }

    private turn() {
        this.httpService.get(`api/game/shoot/${this.opponentId}`).subscribe(resp => {
            this.logService.sendLog(resp[0], resp[1]);

            this.httpService.get(`api/game/result/${this.opponentId}`).subscribe(data => {
                const result = (data as string);
                if (result.length > 0)
                    this.httpService.get(`api/account/${result}`).subscribe(user => {
                        this.hub.invoke("SendAlert", this.opponentId, (user as User).UserName + " won!");
                    });
                else if (result === "Draw")
                    this.hub.invoke("SendAlert", this.opponentId, result);
            });
        });
    }

    stop() {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
        this.sub.unsubscribe();
        this.logService.stop();
        this.httpService.delete("api/game/remove");
        localStorage.removeItem("opponentUserId");
        this.router.navigate([{ outlets: { bottom: ["chat"], general: ["gameInit"] } }], { skipLocationChange: true });
    }

    ngOnDestroy() {
        if (this.connection != null && this.connection.state === 1) {
            this.hub.invoke("Disconnect", localStorage.getItem("opponentUserId"));
            this.stop();
        }
    }
}