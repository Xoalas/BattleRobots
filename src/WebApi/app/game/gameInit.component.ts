﻿import { Actions } from "../models/actions";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { GameService } from "../services/game.service";
import { SearchGameService } from "../services/searchGame.service";
import { LogGameService } from "../services/logGame.service";

@Component({
    selector: "gameInit",
    styleUrls: [`app/game/game.component.css`],
    template: `
<div class="gameOuter">
    <div class="gameInitial">
        <div class="container">
            <h1 *ngIf="points > 0">You still have {{points}} unallocated points</h1>
            <div class="row">
                <div class="rowEl">
                    <input type="image" src="../../img/ForceShield.png" alt="ForceShield" class="img">
                    <label id="ForceShield" class="centered" title="Force Shield, protects against Blaster">{{action.ForceShield}}</label>
                </div>
                <div class="rowEl">
                    <input type="image" src="../../img/Blaster.png" alt="Blaster" class="img">
                    <label id="Blaster" class="centered" title="Blaster, deals 0 to 30 health damage">{{action.Blaster}}</label>
                </div>
            </div>
            <div class="row">
                <div class="rowEl">
                    <input type="image" src="../../img/Maneuver.png" alt="Maneuver" class="img">
                    <label id="Maneuver" class="centered" title="Maneuver, protects against Rocket Launcher">{{action.Maneuver}}</label>
                </div>
                <div class="rowEl">
                    <img src="../../img/confirm.png" alt="Confirm" title="Confirm" id="conf" class="btn img">
                    <img src="../../img/discard.png" alt="Discard" title="Discard" id="disc" class="btn img">
                    <img src="../../img/search.png" alt="Search Game" title="Search Game" id="searchGame" class="btn img">
                </div>
                <div class="rowEl">
                    <input type="image" src="../../img/RocketLauncher.png" alt="RocketLauncher" class="img">
                    <label id="RocketLauncher" class="centered" title="Rocket Launcher, deals 0 to 40 health damage">{{action.RocketLauncher}}</label>
                </div>
            </div>
            <div class="row">
                <div class="rowEl">
                    <input type="image" src="../../img/Interferer.png" alt="Interferer" class="img">
                    <label id="Interferer" class="centered" title="Interferer, protects against Torpedoes">{{action.Interferer}}</label>
                </div>
                <div class="rowEl">
                    <input type="image" src="../../img/Torpedoes.png" alt="Torpedoes" class="img">
                    <label id="Torpedoes" class="centered" title="Torpedoes, deals 0 to 50 health damage">{{action.Torpedoes}}</label>
                </div>
            </div>
        </div>
    </div>
</div>`
})
export class GameInitComponent implements OnInit, OnDestroy {
    private action: Actions;
    private points: number;
    private searchGameHub: SignalR.Hub.Proxy;

    constructor(private gameService: GameService,
        private searchGameService: SearchGameService,
        private logService: LogGameService) {
    }

    ngOnInit() {
        this.init();
        $("#disc").hide();
        $("#searchGame").hide();

        $(document).ready(() => {
            $("#conf").click(() => {
                if (this.points > 0)
                    alert(`You still have ${this.points} unallocated points`);
                else {
                    $(".centered, .img").attr("disabled", "disabled").off("mousedown");
                    $("#conf").hide();
                    $("#disc, #searchGame").show();
                    this.searchGameHub.invoke("DoConnect");
                }
            });
            $("#disc").on("click",
                () => {
                    this.searchGameService.stop();
                    this.init();
                    $(".gameInitial *").prop("disabled", false);
                    $("#conf").show();
                    $("#disc, #searchGame").hide();
                    $(".subtractAction").prop("disabled", true);
                });
            $("#searchGame").on("click",
                () => {
                    this.searchGameHub.invoke("SearchGame", localStorage.getItem("userId"));
                });
        });
    }

    private init() {
        this.gameService.init();
        this.logService.init();
        this.logService.start();
        this.searchGameService.init();
        this.action = this.gameService.getAction();
        this.points = this.gameService.getPoint();
        this.searchGameHub = this.searchGameService.getHub();
        this.searchGameService.start();
        this.equipLoading();
    }

    private equipLoading() {
        $("#ForceShield").mousedown(event => {
            switch (event.which) {
            case 1:
                if (this.points > 0 && this.action.ForceShield !== 3) {
                    this.action.ForceShield += 1;
                    this.points -= 1;
                }
                break;
            case 3:
                if (this.action.ForceShield > 0) {
                    this.action.ForceShield -= 1;
                    this.points += 1;
                }
                break;
            }
        });
        $("#Maneuver").mousedown(event => {
            switch (event.which) {
            case 1:
                if (this.points > 0 && this.action.Maneuver !== 3) {
                    this.action.Maneuver += 1;
                    this.points -= 1;
                }
                break;
            case 3:
                if (this.action.Maneuver > 0) {
                    this.action.Maneuver -= 1;
                    this.points += 1;
                }
                break;
            }
        });
        $("#Interferer").mousedown(event => {
            switch (event.which) {
            case 1:
                if (this.points > 0 && this.action.Interferer !== 2) {
                    this.action.Interferer += 1;
                    this.points -= 1;
                }
                break;
            case 3:
                if (this.action.Interferer > 0) {
                    this.action.Interferer -= 1;
                    this.points += 1;
                }
                break;
            }
        });
        $("#Blaster").mousedown(event => {
            switch (event.which) {
            case 1:
                if (this.points > 0 && this.action.Blaster !== 3) {
                    this.action.Blaster += 1;
                    this.points -= 1;
                }
                break;
            case 3:
                if (this.action.Blaster > 0) {
                    this.action.Blaster -= 1;
                    this.points += 1;
                }
                break;
            }
        });
        $("#RocketLauncher").mousedown(event => {
            switch (event.which) {
            case 1:
                if (this.points > 0 && this.action.RocketLauncher !== 3) {
                    this.action.RocketLauncher += 1;
                    this.points -= 1;
                }
                break;
            case 3:
                if (this.action.RocketLauncher > 0) {
                    this.action.RocketLauncher -= 1;
                    this.points += 1;
                }
                break;
            }
        });
        $("#Torpedoes").mousedown(event => {
            switch (event.which) {
            case 1:
                if (this.points > 0 && this.action.Torpedoes !== 2) {
                    this.action.Torpedoes += 1;
                    this.points -= 1;
                }
                break;
            case 3:
                if (this.action.Torpedoes > 0) {
                    this.action.Torpedoes -= 1;
                    this.points += 1;
                }
                break;
            }
        });
    }

    ngOnDestroy() {
        this.searchGameService.stop();
    }
}