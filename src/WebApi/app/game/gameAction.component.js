"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../services/http.service");
var logGame_service_1 = require("../services/logGame.service");
var game_service_1 = require("../services/game.service");
var app_constants_1 = require("../app.constants");
var Rx_1 = require("rxjs/Rx");
var GameActionComponent = (function () {
    function GameActionComponent(httpService, router, gameService, logService) {
        this.httpService = httpService;
        this.router = router;
        this.gameService = gameService;
        this.logService = logService;
        this.response = false;
        this.userId = localStorage.getItem("userId");
        this.opponentId = localStorage.getItem("opponentUserId");
        this.ticks = 60;
    }
    GameActionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.logService.unsbuscribe();
        this.action = this.gameService.getAction();
        this.timer = Rx_1.Observable.timer(2000, 1000);
        this.sub = this.timer.subscribe(function (t) { return _this.tickerFunc(); });
        this.httpService.get("api/game/create").subscribe();
        this.connection = $.hubConnection(app_constants_1.configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("GameHub");
        $(window).on("beforeunload", function () {
            _this.hub.invoke("Disconnect", localStorage.getItem("opponentUserId"));
            _this.stop();
        });
        this.hub.on("request", function () {
            if (_this.response === false)
                _this.response = true;
        });
        this.hub.on("callback", function () {
            _this.response = false;
            $(".actions *").prop("disabled", false);
            _this.turn();
        });
        this.hub.on("alert", function (msg) {
            _this.logService.notify(msg);
            _this.stop();
            alert(msg);
        });
        this.hub.on("stop", function () {
            _this.httpService.post("stats/" + localStorage.getItem("opponentUserId"), JSON.stringify(localStorage.getItem("userId")));
            alert(localStorage.getItem("userName") + " won!");
            _this.stop();
        });
        this.connection.start().done;
    };
    GameActionComponent.prototype.tickerFunc = function () {
        if (--this.ticks === 0) {
            this.hub.invoke("Disconnect", localStorage.getItem("opponentUserId"));
            this.stop();
            alert("Time for turn has expired. You lose");
        }
    };
    GameActionComponent.prototype.equip = function (equipment) {
        $(".actions *").prop("disabled", true);
        this.ticks = 60;
        this.httpService.post("api/game/equip/" + equipment);
        if (this.response === false) {
            this.response = true;
            this.hub.invoke("SendRequest", this.opponentId);
        }
        else {
            $(".actions *").prop("disabled", false);
            this.response = false;
            this.hub.invoke("SendCallback", this.opponentId);
            this.turn();
        }
    };
    GameActionComponent.prototype.turn = function () {
        var _this = this;
        this.httpService.get("api/game/shoot/" + this.opponentId).subscribe(function (resp) {
            _this.logService.sendLog(resp[0], resp[1]);
            _this.httpService.get("api/game/result/" + _this.opponentId).subscribe(function (data) {
                var result = data;
                if (result.length > 0)
                    _this.httpService.get("api/account/" + result).subscribe(function (user) {
                        _this.hub.invoke("SendAlert", _this.opponentId, user.UserName + " won!");
                    });
                else if (result === "Draw")
                    _this.hub.invoke("SendAlert", _this.opponentId, result);
            });
        });
    };
    GameActionComponent.prototype.stop = function () {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
        this.sub.unsubscribe();
        this.logService.stop();
        this.httpService.delete("api/game/remove");
        localStorage.removeItem("opponentUserId");
        this.router.navigate([{ outlets: { bottom: ["chat"], general: ["gameInit"] } }], { skipLocationChange: true });
    };
    GameActionComponent.prototype.ngOnDestroy = function () {
        if (this.connection != null && this.connection.state === 1) {
            this.hub.invoke("Disconnect", localStorage.getItem("opponentUserId"));
            this.stop();
        }
    };
    return GameActionComponent;
}());
GameActionComponent = __decorate([
    core_1.Component({
        selector: "gameAction",
        styleUrls: ["app/game/game.component.css"],
        template: "\n<div class=\"gameOuter\">\n    <div class=\"gameAction\">\n        <div class=\"actions container\">\n            <div class=\"row\">\n                <div class=\"rowEl\"></div>\n                <div class=\"rowEl\">\n                    <div *ngIf=\"action.ForceShield > 0\">\n                        <input type=\"image\" src=\"../../img/ForceShield.png\" id=\"ForceShield\" (click)=\"action.ForceShield = action.ForceShield - 1; equip('ForceShield')\" alt=\"ForceShield\" class=\"img\">\n                        <label for=\"ForceShield\" class=\"centered\" title=\"Force Shield, protects against Blaster\">{{action.ForceShield}}</label>\n                    </div>\n                </div>\n                <div class=\"rowEl\">\n                    <div *ngIf=\"action.Maneuver > 0\">\n                        <input type=\"image\" src=\"../../img/Maneuver.png\" id=\"Maneuver\" (click)=\"action.Maneuver = action.Maneuver - 1; equip('Maneuver')\" alt=\"Maneuver\" class=\"img\">\n                        <label for=\"Maneuver\" class=\"centered\" title=\"Maneuver, protects against Rocket Launcher\">{{action.Maneuver}}</label>\n                    </div>\n                </div>\n                <div class=\"rowEl\">\n                    <div *ngIf=\"action.Interferer > 0\">\n                        <input type=\"image\" src=\"../../img/Interferer.png\" id=\"Interferer\" (click)=\"action.Interferer = action.Interferer - 1; equip('Interferer')\" alt=\"Interferer\" class=\"img\">\n                        <label for=\"Interferer\" class=\"centered\" title=\"Interferer, protects against Torpedoes\">{{action.Interferer}}</label>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row\"><div id=\"tmr\">Until the end of the turn left {{ticks}}s</div></div>\n            <div class=\"row\">\n                <div class=\"rowEl\">\n                    <input type=\"image\" src=\"../../img/MachineGun.png\" id=\"MachineGun\" (click)=\"equip('MachineGun')\" alt=\"MachineGun\" class=\"img\">\n                    <label for=\"MachineGun\" class=\"centered\" title=\"Machine Gun, deals 0 to 10 health damage\">&#8734;</label>\n                </div>\n                <div class=\"rowEl\">\n                    <div *ngIf=\"action.Blaster > 0\">\n                        <input type=\"image\" src=\"../../img/Blaster.png\" id=\"Blaster\" (click)=\"action.Blaster = action.Blaster - 1; equip('Blaster')\" alt=\"Blaster\" class=\"img\">\n                        <label for=\"Blaster\" class=\"centered\" title=\"Blaster, deals 0 to 30 health damage\">{{action.Blaster}}</label>\n                    </div>\n                </div>\n                <div class=\"rowEl\">\n                    <div *ngIf=\"action.RocketLauncher > 0\">\n                        <input type=\"image\" src=\"../../img/RocketLauncher.png\" id=\"RocketLauncher\" (click)=\"action.RocketLauncher = action.RocketLauncher - 1; equip('RocketLauncher')\" alt=\"RocketLauncher\" class=\"img\">\n                        <label for=\"RocketLauncher\" class=\"centered\" title=\"Rocket Launcher, deals 0 to 40 health damage\">{{action.RocketLauncher}}</label>\n                    </div>\n                </div>\n                <div class=\"rowEl\">\n                    <div *ngIf=\"action.Torpedoes > 0\">\n                        <input type=\"image\" src=\"../../img/Torpedoes.png\" id=\"Torpedoes\" (click)=\"action.Torpedoes = action.Torpedoes - 1; equip('Torpedoes')\" alt=\"Torpedoes\" class=\"img\">\n                        <label for=\"Torpedoes\" class=\"centered\" title=\"Torpedoes, deals 0 to 50 health damage\">{{action.Torpedoes}}</label>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>",
        providers: [http_service_1.HttpService]
    }),
    __metadata("design:paramtypes", [http_service_1.HttpService,
        router_1.Router,
        game_service_1.GameService,
        logGame_service_1.LogGameService])
], GameActionComponent);
exports.GameActionComponent = GameActionComponent;
//# sourceMappingURL=gameAction.component.js.map