"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var app_module_1 = require("./app.module");
var core_1 = require("@angular/core");
var app_constants_1 = require("./app.constants");
var platform = platform_browser_dynamic_1.platformBrowserDynamic();
if (app_constants_1.configuration.environment.production) {
    core_1.enableProdMode();
}
platform.bootstrapModule(app_module_1.AppModule);
//# sourceMappingURL=main.js.map