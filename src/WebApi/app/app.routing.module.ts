﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

//components
import { AppComponent } from "./app.component"
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { NavComponent } from "./nav/nav.component";
import { ContactComponent } from "./contact/contact.component";
import { ChatComponent } from "./chat/chat.component";
import { PrivateChatComponent } from "./chat/privateChat.component";
import { GameInitComponent } from "./game/gameInit.component";
import { GameActionComponent } from "./game/gameAction.component";

const routes: Routes = [
    { path: "home", component: AppComponent, outlet: "primary" },
    { path: "login", component: LoginComponent, outlet: "primary" },
    { path: "register", component: RegisterComponent, outlet: "primary" },
    { path: "nav", component: NavComponent, outlet: "navBar" },
    { path: "chat", component: ChatComponent, outlet: "bottom" },
    { path: "privateChat/:id", component: PrivateChatComponent, outlet: "bottom" },
    { path: "contact", component: ContactComponent, outlet: "contact" },
    { path: "gameInit", component: GameInitComponent, outlet: "general" },
    { path: "gameAction", component: GameActionComponent, outlet: "general" }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}