﻿export let configuration = {
    baseUrls: {
        server: "/",
        signalr: "http://www.battlerobots.somee.com"
    },
    environment: {
        production: true
    }
}