"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//components
var app_component_1 = require("./app.component");
var login_component_1 = require("./login/login.component");
var register_component_1 = require("./register/register.component");
var nav_component_1 = require("./nav/nav.component");
var contact_component_1 = require("./contact/contact.component");
var chat_component_1 = require("./chat/chat.component");
var privateChat_component_1 = require("./chat/privateChat.component");
var gameInit_component_1 = require("./game/gameInit.component");
var gameAction_component_1 = require("./game/gameAction.component");
var routes = [
    { path: "home", component: app_component_1.AppComponent, outlet: "primary" },
    { path: "login", component: login_component_1.LoginComponent, outlet: "primary" },
    { path: "register", component: register_component_1.RegisterComponent, outlet: "primary" },
    { path: "nav", component: nav_component_1.NavComponent, outlet: "navBar" },
    { path: "chat", component: chat_component_1.ChatComponent, outlet: "bottom" },
    { path: "privateChat/:id", component: privateChat_component_1.PrivateChatComponent, outlet: "bottom" },
    { path: "contact", component: contact_component_1.ContactComponent, outlet: "contact" },
    { path: "gameInit", component: gameInit_component_1.GameInitComponent, outlet: "general" },
    { path: "gameAction", component: gameAction_component_1.GameActionComponent, outlet: "general" }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [
            router_1.RouterModule.forRoot(routes)
        ],
        exports: [
            router_1.RouterModule
        ]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app.routing.module.js.map