﻿import { Component, OnInit, OnDestroy } from "@angular/core";
import { HttpService } from "../services/http.service";
import { ContactService } from "../services/contact.service";
import { RedirectToGame } from "../services/redirectToGame.service";
import { User } from "../models/user";
import { LogGameService } from "../services/logGame.service";

@Component({
    selector: "contact",
    styleUrls: [`app/contact/contact.component.css`],
    template: `
<div class="contactListMain">
    <div id="contacts" class="contactList">
    </div>
</div>`,
    providers: [HttpService]
})
export class ContactComponent implements OnInit, OnDestroy {
    private isSubscribed: boolean = false;

    constructor(private contactService: ContactService,
        private httpService: HttpService,
        private logService: LogGameService,
        private redirectService: RedirectToGame) {
    }

    ngOnInit() {
        this.startSignalR();
    }

    private startSignalR() {
        this.contactService.init();
        this.redirectService.init();
        const hub = this.contactService.getHub();
        hub.on("addContacts",
            (users) => {
                this.drawContacts(users);
            });

        hub.on("onUserDisconnected",
            (users) => {
                this.drawContacts(users);
            });
        this.contactService.start();
    }

    private drawContacts(users: any) {
        const that = this;
        $("#contacts").empty();
        const usersList = $.parseJSON(users);
        for (let i = 0; i < usersList.length; i++) {
            $(`<p class="user" style="cursor:pointer;font-size:16px">${usersList[i]}</p>`).appendTo("#contacts").on(
                "click",
                function(e) {
                    const userName = $(this).clone().children().remove().end().text();
                    if (e.target !== this)
                        return;

                    that.httpService.get(`api/account/name/${userName}`)
                        .subscribe(data => {
                            const user = (data as User);

                            var $el = $(
                                    '<div class="popup" style="border:1px solid black;background:red;cursor:auto;font-size:12px"/>')
                                .append(`<div>Victories: ${user.Victories.toString()}</div><div>Defeats: ${user.Defeats
                                    .toString()}</div>`);

                            if (localStorage.getItem("userName") !== userName) {
                                $el.append("<br/>");
                                if (!that.isSubscribed)
                                    $el.append('<div style="cursor:pointer" class="subscribe">Subscribe</div>').on(
                                        "click",
                                        ".subscribe",
                                        () => {
                                            that.logService.subscribe(user.Id);
                                            that.isSubscribed = true;
                                            $(".subscribe").hide();
                                            alert(`You have been successfully subscribed to ${user.UserName}`);
                                        });
                                else
                                    $el.append('<div style="cursor:pointer" class="unsubscribe">Unsubscribe</div>').on(
                                        "click",
                                        ".unsubscribe",
                                        () => {
                                            that.logService.unsbuscribe();
                                            that.isSubscribed = false;
                                            $(".unsubscribe").hide();
                                            alert(`You have been successfully unsubscribed from ${user.UserName}`);
                                        });

                                if ($("#searchGame").is(":visible"))
                                    $el.append('<div style="cursor:pointer" class="prchat">Invite</div>').unbind().on(
                                        "click",
                                        ".prchat",
                                        () => {
                                            that.redirectService.getConfirmation(user.Id);
                                        });
                            }

                            if ($el.parent().length === 0)
                                $(".user *").remove();

                            $(this).append($el);
                            setTimeout(() => { $(".user *").remove(); }, 10000);
                        });
                });
        }
    }

    ngOnDestroy() {
        this.contactService.stop();
        this.redirectService.stop();
    }
}