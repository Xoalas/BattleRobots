"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_service_1 = require("../services/http.service");
var contact_service_1 = require("../services/contact.service");
var redirectToGame_service_1 = require("../services/redirectToGame.service");
var logGame_service_1 = require("../services/logGame.service");
var ContactComponent = (function () {
    function ContactComponent(contactService, httpService, logService, redirectService) {
        this.contactService = contactService;
        this.httpService = httpService;
        this.logService = logService;
        this.redirectService = redirectService;
        this.isSubscribed = false;
    }
    ContactComponent.prototype.ngOnInit = function () {
        this.startSignalR();
    };
    ContactComponent.prototype.startSignalR = function () {
        var _this = this;
        this.contactService.init();
        this.redirectService.init();
        var hub = this.contactService.getHub();
        hub.on("addContacts", function (users) {
            _this.drawContacts(users);
        });
        hub.on("onUserDisconnected", function (users) {
            _this.drawContacts(users);
        });
        this.contactService.start();
    };
    ContactComponent.prototype.drawContacts = function (users) {
        var that = this;
        $("#contacts").empty();
        var usersList = $.parseJSON(users);
        for (var i = 0; i < usersList.length; i++) {
            $("<p class=\"user\" style=\"cursor:pointer;font-size:16px\">" + usersList[i] + "</p>").appendTo("#contacts").on("click", function (e) {
                var _this = this;
                var userName = $(this).clone().children().remove().end().text();
                if (e.target !== this)
                    return;
                that.httpService.get("api/account/name/" + userName)
                    .subscribe(function (data) {
                    var user = data;
                    var $el = $('<div class="popup" style="border:1px solid black;background:red;cursor:auto;font-size:12px"/>')
                        .append("<div>Victories: " + user.Victories.toString() + "</div><div>Defeats: " + user.Defeats
                        .toString() + "</div>");
                    if (localStorage.getItem("userName") !== userName) {
                        $el.append("<br/>");
                        if (!that.isSubscribed)
                            $el.append('<div style="cursor:pointer" class="subscribe">Subscribe</div>').on("click", ".subscribe", function () {
                                that.logService.subscribe(user.Id);
                                that.isSubscribed = true;
                                $(".subscribe").hide();
                                alert("You have been successfully subscribed to " + user.UserName);
                            });
                        else
                            $el.append('<div style="cursor:pointer" class="unsubscribe">Unsubscribe</div>').on("click", ".unsubscribe", function () {
                                that.logService.unsbuscribe();
                                that.isSubscribed = false;
                                $(".unsubscribe").hide();
                                alert("You have been successfully unsubscribed from " + user.UserName);
                            });
                        if ($("#searchGame").is(":visible"))
                            $el.append('<div style="cursor:pointer" class="prchat">Invite</div>').unbind().on("click", ".prchat", function () {
                                that.redirectService.getConfirmation(user.Id);
                            });
                    }
                    if ($el.parent().length === 0)
                        $(".user *").remove();
                    $(_this).append($el);
                    setTimeout(function () { $(".user *").remove(); }, 10000);
                });
            });
        }
    };
    ContactComponent.prototype.ngOnDestroy = function () {
        this.contactService.stop();
        this.redirectService.stop();
    };
    return ContactComponent;
}());
ContactComponent = __decorate([
    core_1.Component({
        selector: "contact",
        styleUrls: ["app/contact/contact.component.css"],
        template: "\n<div class=\"contactListMain\">\n    <div id=\"contacts\" class=\"contactList\">\n    </div>\n</div>",
        providers: [http_service_1.HttpService]
    }),
    __metadata("design:paramtypes", [contact_service_1.ContactService,
        http_service_1.HttpService,
        logGame_service_1.LogGameService,
        redirectToGame_service_1.RedirectToGame])
], ContactComponent);
exports.ContactComponent = ContactComponent;
//# sourceMappingURL=contact.component.js.map