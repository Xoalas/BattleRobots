﻿import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";

@Component({
    selector: "nav",
    styleUrls: [`app/nav/nav.component.css`],
    template: `
<div class="navHeader">
    <div class="navInner">
        <a class="left">Welcome, {{userName}}</a>
        <a class="left" id="gRules">Rules</a>
        <a class="right" id="lOut" (click)="logOut()">LOG OUT</a>
    </div>
</div>
`
})
export class NavComponent implements OnInit {
    private userName = localStorage.getItem("userName");

    constructor(private authService: AuthService) {}

    ngOnInit() {
        $("#gRules").on("click",
            () => {
                alert("To be able to play, select all unallocated points on the equipment.\n" +
                    "A yellow shield (Force Shield) protects from the Blaster. You can choose 3 units of such equipments.\n" +
                    "The orange shield (Maneuver) protects from the Rocket launcher. You can also choose 3 units of such equipments.\n" +
                    "The blue shield (Interferer) protects against Torpedoes. You can select only 2 units of such equipments.\n\n" +
                    'After selecting all unallocated points, you can select a game with a specific user by clicking on his nickname on the right side of the screen or clicking on the "magnifying glass" and a ready-to-play player will be notified.\n' +
                    "If the player approves your offer you will be moved to the game screen.To make a move you must choose either a shield to defend against enemy attacks or weapon that you will attack the enemy");
            });
    }

    private logOut() {
        if (localStorage.getItem("access_token"))
            this.authService.signOut();
    }
}