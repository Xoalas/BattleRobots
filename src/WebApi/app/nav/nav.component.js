"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("../services/auth.service");
var NavComponent = (function () {
    function NavComponent(authService) {
        this.authService = authService;
        this.userName = localStorage.getItem("userName");
    }
    NavComponent.prototype.ngOnInit = function () {
        $("#gRules").on("click", function () {
            alert("To be able to play, select all unallocated points on the equipment.\n" +
                "A yellow shield (Force Shield) protects from the Blaster. You can choose 3 units of such equipments.\n" +
                "The orange shield (Maneuver) protects from the Rocket launcher. You can also choose 3 units of such equipments.\n" +
                "The blue shield (Interferer) protects against Torpedoes. You can select only 2 units of such equipments.\n\n" +
                'After selecting all unallocated points, you can select a game with a specific user by clicking on his nickname on the right side of the screen or clicking on the "magnifying glass" and a ready-to-play player will be notified.\n' +
                "If the player approves your offer you will be moved to the game screen.To make a move you must choose either a shield to defend against enemy attacks or weapon that you will attack the enemy");
        });
    };
    NavComponent.prototype.logOut = function () {
        if (localStorage.getItem("access_token"))
            this.authService.signOut();
    };
    return NavComponent;
}());
NavComponent = __decorate([
    core_1.Component({
        selector: "nav",
        styleUrls: ["app/nav/nav.component.css"],
        template: "\n<div class=\"navHeader\">\n    <div class=\"navInner\">\n        <a class=\"left\">Welcome, {{userName}}</a>\n        <a class=\"left\" id=\"gRules\">Rules</a>\n        <a class=\"right\" id=\"lOut\" (click)=\"logOut()\">LOG OUT</a>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], NavComponent);
exports.NavComponent = NavComponent;
//# sourceMappingURL=nav.component.js.map