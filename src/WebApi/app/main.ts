﻿import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { AppModule } from "./app.module";
import { enableProdMode } from "@angular/core";
import { configuration } from "./app.constants";

const platform = platformBrowserDynamic();

if (configuration.environment.production) {
    enableProdMode();
}

platform.bootstrapModule(AppModule);