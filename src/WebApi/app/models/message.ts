﻿export class Message {
    Id: string;
    Text: string;
    UserId: string;
    ChatId: number;
}