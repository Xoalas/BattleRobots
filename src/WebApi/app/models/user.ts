﻿export class User {
    Id: string;
    UserName: string;
    Victories: number; 
    Defeats: number;
}