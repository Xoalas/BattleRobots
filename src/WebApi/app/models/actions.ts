﻿export class Actions {
    Blaster: number = 0;
    RocketLauncher: number = 0;
    MachineGun: number = 0;
    Torpedoes: number = 0;
    ForceShield: number = 0;
    Maneuver: number = 0;
    Interferer: number = 0;
}