"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Actions = (function () {
    function Actions() {
        this.Blaster = 0;
        this.RocketLauncher = 0;
        this.MachineGun = 0;
        this.Torpedoes = 0;
        this.ForceShield = 0;
        this.Maneuver = 0;
        this.Interferer = 0;
    }
    return Actions;
}());
exports.Actions = Actions;
//# sourceMappingURL=actions.js.map