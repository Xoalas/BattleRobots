﻿/// <reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../node_modules/@types/signalr/index.d.ts" />

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: "my-app",
    styleUrls: [`app/app.component.css?${new Date().getTime().toString()}`],
    template: `
<div class="appContainer">
    <div class="appTop">
        <router-outlet name="navBar"></router-outlet>
    </div>
    <div class="appLeft">
        <div class="appCenter">
            <router-outlet name="general"></router-outlet>
        </div>
        <div class="appBottom">
            <router-outlet name="bottom"></router-outlet>
        </div>
    </div>
    <div class="appRight">
        <router-outlet name="contact"></router-outlet>
    </div>    
</div>
<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {
        document.oncontextmenu = () => false;
        document.onselectstart = () => false;

        if (!localStorage.getItem("access_token"))
            this.router.navigate(
                [{ outlets: { primary: ["login"], navBar: null, contact: null, general: null, bottom: null } }],
                { skipLocationChange: true });
        else
            this.router.navigate([
                    {
                        outlets: {
                            primary: null,
                            navBar: ["nav"],
                            contact: ["contact"],
                            general: ["gameInit"],
                            bottom: ["chat"]
                        }
                    }
                ],
                { skipLocationChange: true });
    }
}