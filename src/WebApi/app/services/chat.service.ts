﻿import { Injectable } from "@angular/core";
import { configuration } from "../app.constants";

@Injectable()
export class ChatService {
    private userName = localStorage.getItem("userName");
    private connection: SignalR.Hub.Connection;

    start() {
        this.connection = $.hubConnection(configuration.baseUrls.signalr);
        const hub = this.connection.createHubProxy("PublicChatHub");

        hub.on("addMessage",
            (name, message) => {
                const align = (name == this.userName) ? "right" : "left";
                $("<div/>",
                    {
                        text: name + ": " + message,
                        css: {
                            "text-align": align
                        }
                    }).append("<br/><br/>").appendTo("#log");
                $("#log").scrollTop($("#log")[0].scrollHeight);
            });

        this.connection.start().done(() => {
            $("#msg").on("keyup",
                e => {
                    if (e.which === 13) {
                        const msg = $("#msg").val();
                        hub.invoke("Send", this.userName, msg);
                        $("#msg").val("");
                    }
                });
        });
    }

    stop() {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    }
}