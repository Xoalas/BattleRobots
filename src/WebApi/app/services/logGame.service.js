"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_service_1 = require("./http.service");
var app_constants_1 = require("../app.constants");
var LogGameService = (function () {
    function LogGameService(httpService) {
        this.httpService = httpService;
    }
    LogGameService.prototype.init = function () {
        var _this = this;
        this.connection = $.hubConnection(app_constants_1.configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("LogHub");
        this.hub.on("addLog", function (userId, message) {
            _this.httpService.get("api/account/" + userId).subscribe(function (data) {
                $("<div style=\"color:#4e93e8;font-size:12px;\">" + data.UserName + " " + message + "</div><br/>")
                    .appendTo("#log");
                $("#log").scrollTop($("#log")[0].scrollHeight);
            });
        });
        this.hub.on("notify", function (message) {
            $("<div style=\"color:red;font-size:14px;\">" + message + "</div><br/>").appendTo("#log");
            $("#log").scrollTop($("#log")[0].scrollHeight);
        });
    };
    LogGameService.prototype.getHub = function () {
        return this.hub;
    };
    LogGameService.prototype.sendLog = function (userId, msg) {
        this.hub.invoke("SendLog", userId, localStorage.getItem("userId"), localStorage.getItem("opponentUserId"), msg);
    };
    LogGameService.prototype.subscribe = function (userId) {
        this.hub.invoke("Subscribe", localStorage.getItem("userId"), userId);
    };
    LogGameService.prototype.unsbuscribe = function () {
        this.hub.invoke("Unsubscribe", localStorage.getItem("userId"));
    };
    LogGameService.prototype.notify = function (msg) {
        this.hub.invoke("SendNotification", localStorage.getItem("userId"), localStorage.getItem("opponentUserId"), msg);
    };
    LogGameService.prototype.start = function () {
        this.stop();
        this.connection.start().done;
    };
    LogGameService.prototype.stop = function () {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    };
    return LogGameService;
}());
LogGameService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_service_1.HttpService])
], LogGameService);
exports.LogGameService = LogGameService;
//# sourceMappingURL=logGame.service.js.map