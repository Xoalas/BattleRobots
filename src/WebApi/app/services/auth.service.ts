﻿import { configuration } from "../app.constants";
import { Injectable } from "@angular/core";
import { Response, Http, Headers } from "@angular/http";
import { LoginForm } from "../models/loginForm";
import { Router } from "@angular/router";
import { User } from "../models/user";

@Injectable()
export class AuthService {

    constructor(private http: Http, private router: Router) {}

    signIn(url: string, form: LoginForm) {
        const body = `username=${form.UserName}&password=${form.Password}&grant_type=password`;
        const headers = new Headers({ 'Content-Type': "application/x-www-form-urlencoded" });

        return this.http.post(configuration.baseUrls.server + url, body, { headers: headers }).subscribe(
            response => {
                localStorage.setItem("access_token", response.json().access_token);
                localStorage.setItem("expires_in", response.json().expires_in);
                localStorage.setItem("token_type", response.json().token_type);
                localStorage.setItem("userName", response.json().userName);
                this.http.get(configuration.baseUrls.server + "api/account/current",
                    {
                        headers: new Headers({
                            'Content-Type': "application/json",
                            'Authorization': `Bearer ${response.json().access_token}`
                        })
                    }).map((resp: Response) => resp.json()).subscribe((data) => {
                    localStorage.setItem("userId", (data as User).Id);
                    this.router.navigate([{ outlets: { primary: ["home"] } }], { skipLocationChange: true });
                });
            },
            error => {
                $("#loginErr").text("Wrong login or password. Please try again").show();
                setTimeout(() => { $("#loginErr").hide(); }, 5000);
            }
        );
    }

    signUp(url: string, body: string) {
        const headers = new Headers({ 'Content-Type': "application/json" });
        return this.http.post(configuration.baseUrls.server + url, body, { headers: headers }).subscribe(
            response => {
                this.router.navigate([{ outlets: { primary: ["login"] } }], { skipLocationChange: true });
            },
            error => {
                $("#regErr").text("An error occurred while registering. Please try again or use a different login")
                    .show();
                setTimeout(() => { $("#regErr").hide(); }, 5000);
            }
        );
    }

    signOut() {
        localStorage.removeItem("access_token");
        localStorage.removeItem("expires_in");
        localStorage.removeItem("token_type");
        localStorage.removeItem("userName");
        localStorage.removeItem("userId");
        this.router.navigate(
            [{ outlets: { primary: ["login"], navBar: null, contact: null, general: null, bottom: null } }],
            { skipLocationChange: true });
    }
}