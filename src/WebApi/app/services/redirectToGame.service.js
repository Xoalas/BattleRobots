"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_constants_1 = require("../app.constants");
var http_service_1 = require("./http.service");
var router_1 = require("@angular/router");
var RedirectToGame = (function () {
    function RedirectToGame(httpService, router) {
        this.httpService = httpService;
        this.router = router;
    }
    RedirectToGame.prototype.init = function () {
        var _this = this;
        this.connection = $.hubConnection(app_constants_1.configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("RedirectHub");
        this.hub.on("request", function (userId, userName) {
            var isAgree = false;
            if ($("#searchGame").is(":visible"))
                isAgree = confirm(userName + " wants you to join him in the game");
            if (isAgree)
                _this.connectToGame(userId);
            _this.hub.invoke("SendCallBack", localStorage.getItem("userId"), userId, localStorage.getItem("userName"), isAgree);
        });
        this.hub.on("callback", function (userId, userName, isAgree) {
            if (isAgree)
                _this.connectToGame(userId);
            else {
                $("#searchGame, #disc").show();
                alert(userName + " denied your request");
            }
        });
        this.start();
    };
    RedirectToGame.prototype.connectToGame = function (userId) {
        var _this = this;
        this.httpService.get("api/chats/user/" + userId).subscribe(function (data) {
            localStorage.setItem("opponentUserId", userId);
            var chat = data;
            _this.router.navigate([{ outlets: { bottom: ["privateChat", chat.Id], general: ["gameAction"] } }], { skipLocationChange: true });
        });
    };
    RedirectToGame.prototype.getConfirmation = function (userId) {
        $("#searchGame, #disc").hide();
        this.hub.invoke("SendRequest", localStorage.getItem("userId"), userId, localStorage.getItem("userName"));
    };
    RedirectToGame.prototype.getHub = function () {
        return this.hub;
    };
    RedirectToGame.prototype.start = function () {
        this.stop();
        this.connection.start().done;
    };
    RedirectToGame.prototype.stop = function () {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    };
    return RedirectToGame;
}());
RedirectToGame = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_service_1.HttpService, router_1.Router])
], RedirectToGame);
exports.RedirectToGame = RedirectToGame;
//# sourceMappingURL=redirectToGame.service.js.map