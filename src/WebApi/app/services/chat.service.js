"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_constants_1 = require("../app.constants");
var ChatService = (function () {
    function ChatService() {
        this.userName = localStorage.getItem("userName");
    }
    ChatService.prototype.start = function () {
        var _this = this;
        this.connection = $.hubConnection(app_constants_1.configuration.baseUrls.signalr);
        var hub = this.connection.createHubProxy("PublicChatHub");
        hub.on("addMessage", function (name, message) {
            var align = (name == _this.userName) ? "right" : "left";
            $("<div/>", {
                text: name + ": " + message,
                css: {
                    "text-align": align
                }
            }).append("<br/><br/>").appendTo("#log");
            $("#log").scrollTop($("#log")[0].scrollHeight);
        });
        this.connection.start().done(function () {
            $("#msg").on("keyup", function (e) {
                if (e.which === 13) {
                    var msg = $("#msg").val();
                    hub.invoke("Send", _this.userName, msg);
                    $("#msg").val("");
                }
            });
        });
    };
    ChatService.prototype.stop = function () {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    };
    return ChatService;
}());
ChatService = __decorate([
    core_1.Injectable()
], ChatService);
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map