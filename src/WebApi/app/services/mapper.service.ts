﻿import { Injectable } from "@angular/core";
import { HttpService } from "../services/http.service";

@Injectable()
export class MapperService {
    constructor(private httpService: HttpService) {}

    mapIdToNameInActiveChat(): Map<string, string> {
        const map = new Map<string, string>();
        this.httpService.get(`api/chats/${localStorage.getItem("chatId")}/members`).subscribe(data => {
            for (let member of data) {
                map.set(member.Id, member.UserName);
            }
        });
        return map;
    }
}