"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_constants_1 = require("../app.constants");
var redirectToGame_service_1 = require("../services/redirectToGame.service");
var SearchGameService = (function () {
    function SearchGameService(redirectService) {
        this.redirectService = redirectService;
    }
    SearchGameService.prototype.init = function () {
        var _this = this;
        this.connection = $.hubConnection(app_constants_1.configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("SearchGameHub");
        this.hub.connection.qs = { 'UserId': localStorage.getItem("userId") };
        this.hub.on("getPlayer", function (id) {
            _this.redirectService.getConfirmation(id);
        });
        this.hub.on("callback", function () {
            alert("Unfortunately, there are no available users to play. Please try again later");
        });
    };
    SearchGameService.prototype.getHub = function () {
        return this.hub;
    };
    SearchGameService.prototype.start = function () {
        this.stop();
        this.connection.start().done;
    };
    SearchGameService.prototype.stop = function () {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    };
    return SearchGameService;
}());
SearchGameService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [redirectToGame_service_1.RedirectToGame])
], SearchGameService);
exports.SearchGameService = SearchGameService;
//# sourceMappingURL=searchGame.service.js.map