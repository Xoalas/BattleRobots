﻿import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { User } from "../models/user";
import { configuration } from "../app.constants";

@Injectable()
export class LogGameService {
    private connection: SignalR.Hub.Connection;
    private hub: SignalR.Hub.Proxy;

    constructor(private httpService: HttpService) {}

    init() {
        this.connection = $.hubConnection(configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("LogHub");
        this.hub.on("addLog",
            (userId, message) => {
                this.httpService.get(`api/account/${userId}`).subscribe(data => {
                    $(`<div style="color:#4e93e8;font-size:12px;">${(data as User).UserName} ${message}</div><br/>`)
                        .appendTo("#log");
                    $("#log").scrollTop($("#log")[0].scrollHeight);
                });
            });
        this.hub.on("notify",
            message => {
                $(`<div style="color:red;font-size:14px;">${message}</div><br/>`).appendTo("#log");
                $("#log").scrollTop($("#log")[0].scrollHeight);
            });
    }

    getHub(): SignalR.Hub.Proxy {
        return this.hub;
    }

    sendLog(userId: string, msg: string) {
        this.hub.invoke("SendLog", userId, localStorage.getItem("userId"), localStorage.getItem("opponentUserId"), msg);
    }

    subscribe(userId: string) {
        this.hub.invoke("Subscribe", localStorage.getItem("userId"), userId);
    }

    unsbuscribe() {
        this.hub.invoke("Unsubscribe", localStorage.getItem("userId"));
    }

    notify(msg: string) {
        this.hub.invoke("SendNotification",
            localStorage.getItem("userId"),
            localStorage.getItem("opponentUserId"),
            msg);
    }

    start() {
        this.stop();
        this.connection.start().done;
    }

    stop() {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    }
}