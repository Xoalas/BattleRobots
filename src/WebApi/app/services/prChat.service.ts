﻿import { Injectable } from "@angular/core";
import { configuration } from "../app.constants";

@Injectable()
export class PrivateChatService {
    private map = new Map<string, string>();
    private opponentId = localStorage.getItem("opponentUserId");
    private selfId = localStorage.getItem("userId");
    private connection: SignalR.Hub.Connection;
    private hub: SignalR.Hub.Proxy;

    init(map: Map<string, string>) {
        this.map = map;
        this.connection = $.hubConnection(configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("PrivateChatHub");

        this.hub.on("addMessage",
            (name, message) => {
                const align = (name == this.selfId) ? "right" : "left";
                $("<div/>",
                    {
                        text: this.map.get(name) + ": " + message,
                        css: {
                            "text-align": align
                        }
                    }).append("<br/><br/>").appendTo("#log");
                $("#log").scrollTop($("#log")[0].scrollHeight);
            });

        $("#msg").on("keyup",
            e => {
                if (e.which === 13) {
                    const msg = $("#msg").val();
                    this.hub.invoke("SendChatMessage", this.selfId, this.opponentId, msg);
                    $("#msg").val("");
                }
            });
    }

    getHub(): SignalR.Hub.Proxy {
        return this.hub;
    }

    start() {
        this.stop();
        this.connection.start().done;
    }

    stop() {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    }
}