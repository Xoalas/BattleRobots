﻿import { Injectable } from "@angular/core";
import { configuration } from "../app.constants";
import { RedirectToGame } from "../services/redirectToGame.service";

@Injectable()
export class SearchGameService {
    private connection: SignalR.Hub.Connection;
    private hub: SignalR.Hub.Proxy;

    constructor(private redirectService: RedirectToGame) {}

    init() {
        this.connection = $.hubConnection(configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("SearchGameHub");
        this.hub.connection.qs = { 'UserId': localStorage.getItem("userId") };
        this.hub.on("getPlayer",
            (id) => {
                this.redirectService.getConfirmation(id);
            });
        this.hub.on("callback",
            () => {
                alert("Unfortunately, there are no available users to play. Please try again later");
            });
    }

    getHub(): SignalR.Hub.Proxy {
        return this.hub;
    }

    start() {
        this.stop();
        this.connection.start().done;
    }

    stop() {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    }
}