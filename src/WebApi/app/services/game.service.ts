﻿import { Injectable } from "@angular/core";
import { Actions } from "../models/actions";

@Injectable()
export class GameService {
    private action: Actions;
    private point: number;

    init() {
        this.action = new Actions();
        this.point = 10;
    }

    getAction() {
        return this.action;
    }

    getPoint(): number {
        return this.point;
    }
}