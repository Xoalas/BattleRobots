"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var app_constants_1 = require("../app.constants");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var AuthService = (function () {
    function AuthService(http, router) {
        this.http = http;
        this.router = router;
    }
    AuthService.prototype.signIn = function (url, form) {
        var _this = this;
        var body = "username=" + form.UserName + "&password=" + form.Password + "&grant_type=password";
        var headers = new http_1.Headers({ 'Content-Type': "application/x-www-form-urlencoded" });
        return this.http.post(app_constants_1.configuration.baseUrls.server + url, body, { headers: headers }).subscribe(function (response) {
            localStorage.setItem("access_token", response.json().access_token);
            localStorage.setItem("expires_in", response.json().expires_in);
            localStorage.setItem("token_type", response.json().token_type);
            localStorage.setItem("userName", response.json().userName);
            _this.http.get(app_constants_1.configuration.baseUrls.server + "api/account/current", {
                headers: new http_1.Headers({
                    'Content-Type': "application/json",
                    'Authorization': "Bearer " + response.json().access_token
                })
            }).map(function (resp) { return resp.json(); }).subscribe(function (data) {
                localStorage.setItem("userId", data.Id);
                _this.router.navigate([{ outlets: { primary: ["home"] } }], { skipLocationChange: true });
            });
        }, function (error) {
            $("#loginErr").text("Wrong login or password. Please try again").show();
            setTimeout(function () { $("#loginErr").hide(); }, 5000);
        });
    };
    AuthService.prototype.signUp = function (url, body) {
        var _this = this;
        var headers = new http_1.Headers({ 'Content-Type': "application/json" });
        return this.http.post(app_constants_1.configuration.baseUrls.server + url, body, { headers: headers }).subscribe(function (response) {
            _this.router.navigate([{ outlets: { primary: ["login"] } }], { skipLocationChange: true });
        }, function (error) {
            $("#regErr").text("An error occurred while registering. Please try again or use a different login")
                .show();
            setTimeout(function () { $("#regErr").hide(); }, 5000);
        });
    };
    AuthService.prototype.signOut = function () {
        localStorage.removeItem("access_token");
        localStorage.removeItem("expires_in");
        localStorage.removeItem("token_type");
        localStorage.removeItem("userName");
        localStorage.removeItem("userId");
        this.router.navigate([{ outlets: { primary: ["login"], navBar: null, contact: null, general: null, bottom: null } }], { skipLocationChange: true });
    };
    return AuthService;
}());
AuthService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map