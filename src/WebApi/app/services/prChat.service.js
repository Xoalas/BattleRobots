"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_constants_1 = require("../app.constants");
var PrivateChatService = (function () {
    function PrivateChatService() {
        this.map = new Map();
        this.opponentId = localStorage.getItem("opponentUserId");
        this.selfId = localStorage.getItem("userId");
    }
    PrivateChatService.prototype.init = function (map) {
        var _this = this;
        this.map = map;
        this.connection = $.hubConnection(app_constants_1.configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("PrivateChatHub");
        this.hub.on("addMessage", function (name, message) {
            var align = (name == _this.selfId) ? "right" : "left";
            $("<div/>", {
                text: _this.map.get(name) + ": " + message,
                css: {
                    "text-align": align
                }
            }).append("<br/><br/>").appendTo("#log");
            $("#log").scrollTop($("#log")[0].scrollHeight);
        });
        $("#msg").on("keyup", function (e) {
            if (e.which === 13) {
                var msg = $("#msg").val();
                _this.hub.invoke("SendChatMessage", _this.selfId, _this.opponentId, msg);
                $("#msg").val("");
            }
        });
    };
    PrivateChatService.prototype.getHub = function () {
        return this.hub;
    };
    PrivateChatService.prototype.start = function () {
        this.stop();
        this.connection.start().done;
    };
    PrivateChatService.prototype.stop = function () {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    };
    return PrivateChatService;
}());
PrivateChatService = __decorate([
    core_1.Injectable()
], PrivateChatService);
exports.PrivateChatService = PrivateChatService;
//# sourceMappingURL=prChat.service.js.map