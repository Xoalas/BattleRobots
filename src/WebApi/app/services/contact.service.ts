﻿import { Injectable } from "@angular/core";
import { configuration } from "../app.constants";

@Injectable()
export class ContactService {
    private connection: SignalR.Hub.Connection;
    private hub: SignalR.Hub.Proxy;

    init() {
        this.connection = $.hubConnection(configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("ContactsHub");
        this.hub.connection.qs = { 'User-Name': localStorage.getItem("userName") };
    }

    getHub(): SignalR.Hub.Proxy {
        return this.hub;
    }

    start() {
        this.stop();
        this.connection.start().done(() => {
            this.hub.invoke("GetContacts");
        });
    }

    stop() {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    }
}