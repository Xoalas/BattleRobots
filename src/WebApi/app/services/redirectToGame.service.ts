﻿import { Injectable } from "@angular/core";
import { configuration } from "../app.constants";
import { HttpService } from "./http.service";
import { Router } from "@angular/router";
import { Chat } from "../models/chat";

@Injectable()
export class RedirectToGame {
    private connection: SignalR.Hub.Connection;
    private hub: SignalR.Hub.Proxy;

    constructor(private httpService: HttpService, private router: Router) {}

    init() {
        this.connection = $.hubConnection(configuration.baseUrls.signalr);
        this.hub = this.connection.createHubProxy("RedirectHub");

        this.hub.on("request",
            (userId, userName) => {
                let isAgree: boolean = false;

                if ($("#searchGame").is(":visible"))
                    isAgree = confirm(userName + " wants you to join him in the game");

                if (isAgree)
                    this.connectToGame(userId);

                this.hub.invoke("SendCallBack",
                    localStorage.getItem("userId"),
                    userId,
                    localStorage.getItem("userName"),
                    isAgree);
            });

        this.hub.on("callback",
            (userId, userName, isAgree) => {
                if (isAgree)
                    this.connectToGame(userId);
                else {
                    $("#searchGame, #disc").show();
                    alert(userName + " denied your request");
                }
            });

        this.start();
    }

    private connectToGame(userId: string) {
        this.httpService.get(`api/chats/user/${userId}`).subscribe(data => {
            localStorage.setItem("opponentUserId", userId);
            const chat = (data as Chat);
            this.router.navigate([{ outlets: { bottom: ["privateChat", chat.Id], general: ["gameAction"] } }],
                { skipLocationChange: true });
        });
    }

    getConfirmation(userId: string) {
        $("#searchGame, #disc").hide();
        this.hub.invoke("SendRequest", localStorage.getItem("userId"), userId, localStorage.getItem("userName"));
    }

    getHub(): SignalR.Hub.Proxy {
        return this.hub;
    }

    start() {
        this.stop();
        this.connection.start().done;
    }

    stop() {
        if (this.connection != null && this.connection.state === 1)
            this.connection.stop();
    }
}