﻿import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { AppRoutingModule } from "./app.routing.module";

//services
import { HttpService } from "./services/http.service";
import { AuthService } from "./services/auth.service";
import { ChatService } from "./services/chat.service";
import { PrivateChatService } from "./services/prChat.service";
import { GameService } from "./services/game.service";
import { SearchGameService } from "./services/searchGame.service";
import { RedirectToGame } from "./services/redirectToGame.service";
import { ContactService } from "./services/contact.service";
import { MapperService } from "./services/mapper.service";
import { LogGameService } from "./services/logGame.service";

//components
import { AppComponent } from "./app.component"
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { NavComponent } from "./nav/nav.component";
import { ContactComponent } from "./contact/contact.component";
import { ChatComponent } from "./chat/chat.component";
import { PrivateChatComponent } from "./chat/privateChat.component";
import { GameInitComponent } from "./game/gameInit.component";
import { GameActionComponent } from "./game/gameAction.component";

@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, AppRoutingModule],
    declarations: [
        AppComponent, LoginComponent, RegisterComponent, NavComponent, ContactComponent,
        ChatComponent, PrivateChatComponent, GameInitComponent, GameActionComponent
    ],
    providers: [
        HttpService, AuthService, PrivateChatService, GameService, MapperService,
        SearchGameService, RedirectToGame, ChatService, ContactService, LogGameService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}