"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var router_1 = require("@angular/router");
var testing_3 = require("@angular/router/testing");
var app_component_1 = require("../../app.component");
// First, initialize the Angular testing environment.
testing_1.getTestBed().initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
var MockRouter = (function () {
    function MockRouter() {
        this.navigate = jasmine.createSpy("navigate");
    }
    return MockRouter;
}());
describe("Component: AppComponent", function () {
    var component;
    //updated beforeEach
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                testing_3.RouterTestingModule
            ],
            declarations: [app_component_1.AppComponent],
            providers: [
                { provide: router_1.Router, useClass: MockRouter }
            ]
        }).compileComponents().then(function () {
            var fixture = testing_1.TestBed.createComponent(app_component_1.AppComponent);
            component = fixture.componentInstance;
        });
        // Mock localStorage
        var store = {};
        spyOn(localStorage, "getItem").and.callFake(function (key) {
            return store[key] || null;
        });
        spyOn(localStorage, "removeItem").and.callFake(function (key) {
            delete store[key];
        });
        spyOn(localStorage, "setItem").and.callFake(function (key, value) {
            return store[key] = value;
        });
        spyOn(localStorage, "clear").and.callFake(function () {
            store = {};
        });
    }));
    //tests
    describe("ngOnInit", function () {
        it('should call Router.navigate([{ outlets: { primary: ["login"], navBar: null, contact: null, general: null, bottom: null } }], { skipLocationChange: true }) when token does not setted to the localStorage', testing_1.inject([router_1.Router], function (router) {
            component.ngOnInit();
            expect(router.navigate).toHaveBeenCalledWith([
                {
                    outlets: {
                        primary: ["login"],
                        navBar: null,
                        contact: null,
                        general: null,
                        bottom: null
                    }
                }
            ], { skipLocationChange: true });
        }));
        it('should call Router.navigate([{ outlets: { primary: null, navBar: ["nav"], contact: ["contact"], general: ["gameInit"], bottom: ["chat"] } }], { skipLocationChange: true }) when token does not setted to the localStorage', testing_1.inject([router_1.Router], function (router) {
            localStorage.setItem("access_token", "access_token");
            component.ngOnInit();
            expect(router.navigate).toHaveBeenCalledWith([
                {
                    outlets: {
                        primary: null,
                        navBar: ["nav"],
                        contact: ["contact"],
                        general: ["gameInit"],
                        bottom: ["chat"]
                    }
                }
            ], { skipLocationChange: true });
        }));
    });
});
//# sourceMappingURL=app.spec.js.map