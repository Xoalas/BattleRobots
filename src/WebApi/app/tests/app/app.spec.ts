﻿import { async, inject, TestBed, getTestBed } from "@angular/core/testing";
import {
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting
    } from "@angular/platform-browser-dynamic/testing";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { AppComponent } from "../../app.component";

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting()
);

class MockRouter {
    navigate = jasmine.createSpy("navigate");
}

describe("Component: AppComponent",
    () => {
        let component: AppComponent;

        //updated beforeEach
        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [
                    RouterTestingModule
                ],
                declarations: [AppComponent],
                providers: [
                    { provide: Router, useClass: MockRouter }
                ]
            }).compileComponents().then(() => {
                const fixture = TestBed.createComponent(AppComponent);

                component = fixture.componentInstance;
            });

            // Mock localStorage
            var store = {};

            spyOn(localStorage, "getItem").and.callFake((key: string): String => {
                return store[key] || null;
            });
            spyOn(localStorage, "removeItem").and.callFake((key: string): void => {
                delete store[key];
            });
            spyOn(localStorage, "setItem").and.callFake((key: string, value: string): string => {
                return store[key] = (value as string);
            });
            spyOn(localStorage, "clear").and.callFake(() => {
                store = {};
            });
        }));

        //tests
        describe("ngOnInit",
            () => {
                it(
                    'should call Router.navigate([{ outlets: { primary: ["login"], navBar: null, contact: null, general: null, bottom: null } }], { skipLocationChange: true }) when token does not setted to the localStorage',
                    inject([Router],
                        (router: Router) => {
                            component.ngOnInit();

                            expect(router.navigate).toHaveBeenCalledWith([
                                    {
                                        outlets: {
                                            primary: ["login"],
                                            navBar: null,
                                            contact: null,
                                            general: null,
                                            bottom: null
                                        }
                                    }
                                ],
                                { skipLocationChange: true });
                        }));

                it(
                    'should call Router.navigate([{ outlets: { primary: null, navBar: ["nav"], contact: ["contact"], general: ["gameInit"], bottom: ["chat"] } }], { skipLocationChange: true }) when token does not setted to the localStorage',
                    inject([Router],
                        (router: Router) => {
                            localStorage.setItem("access_token", "access_token");
                            component.ngOnInit();
                            expect(router.navigate).toHaveBeenCalledWith([
                                    {
                                        outlets: {
                                            primary: null,
                                            navBar: ["nav"],
                                            contact: ["contact"],
                                            general: ["gameInit"],
                                            bottom: ["chat"]
                                        }
                                    }
                                ],
                                { skipLocationChange: true });
                        }));
            });
    });