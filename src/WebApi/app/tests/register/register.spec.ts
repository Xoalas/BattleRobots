﻿import { FormsModule } from "@angular/forms";
import { ComponentFixture, async, inject, TestBed, getTestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { By } from "@angular/platform-browser";
import { RegisterComponent } from "../../register/register.component";
import { AuthService } from "../../services/auth.service";

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting()
);

class MockAuthService {
    signUp = jasmine.createSpy("signUp");
}

describe("Component: RegisterComponent",
    () => {
        let component: RegisterComponent;
        let fixture: ComponentFixture<RegisterComponent>;

        //updated beforeEach
        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [FormsModule],
                declarations: [RegisterComponent],
                providers: [
                    { provide: AuthService, useClass: MockAuthService }
                ]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(RegisterComponent);
                component = fixture.componentInstance;
            });
        }));

        //tests
        describe("(click)",
            () => {
                it("should call signUp on click",
                    inject([AuthService],
                        (service: AuthService) => {
                            fixture.nativeElement.querySelector("#signUp").click();
                            fixture.detectChanges();
                            expect(service.signUp).toHaveBeenCalledTimes(1);
                        }));

                it("should show error msg when passwords do not match",
                    () => {
                        fixture.detectChanges();
                        const p = fixture.debugElement.query(By.css("#regErr")).nativeElement;
                        const pass = fixture.debugElement.query(By.css("#pass")).nativeElement;
                        pass.value = "Test1";
                        pass.dispatchEvent(new Event("input"));

                        const cpass = fixture.debugElement.query(By.css("#cpass")).nativeElement;
                        cpass.value = "Test2";
                        cpass.dispatchEvent(new Event("input"));

                        fixture.nativeElement.querySelector("#signUp").click();
                        fixture.whenStable().then(() => {
                            expect(p.textContent).toEqual("Passwords do not match");
                        });
                    });
            });

        describe("onKeypress",
            () => {
                it("should invoke signUp on keypress",
                    inject([AuthService],
                        (authService: AuthService) => {
                            const input = fixture.debugElement.query(By.css("#cpass"));
                            input.triggerEventHandler("keyup.enter", {});
                            fixture.detectChanges();
                            expect(authService.signUp).toHaveBeenCalledTimes(1);
                        }));
            });
    });