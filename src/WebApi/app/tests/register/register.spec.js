"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var forms_1 = require("@angular/forms");
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var platform_browser_1 = require("@angular/platform-browser");
var register_component_1 = require("../../register/register.component");
var auth_service_1 = require("../../services/auth.service");
// First, initialize the Angular testing environment.
testing_1.getTestBed().initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
var MockAuthService = (function () {
    function MockAuthService() {
        this.signUp = jasmine.createSpy("signUp");
    }
    return MockAuthService;
}());
describe("Component: RegisterComponent", function () {
    var component;
    var fixture;
    //updated beforeEach
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [forms_1.FormsModule],
            declarations: [register_component_1.RegisterComponent],
            providers: [
                { provide: auth_service_1.AuthService, useClass: MockAuthService }
            ]
        }).compileComponents().then(function () {
            fixture = testing_1.TestBed.createComponent(register_component_1.RegisterComponent);
            component = fixture.componentInstance;
        });
    }));
    //tests
    describe("(click)", function () {
        it("should call signUp on click", testing_1.inject([auth_service_1.AuthService], function (service) {
            fixture.nativeElement.querySelector("#signUp").click();
            fixture.detectChanges();
            expect(service.signUp).toHaveBeenCalledTimes(1);
        }));
        it("should show error msg when passwords do not match", function () {
            fixture.detectChanges();
            var p = fixture.debugElement.query(platform_browser_1.By.css("#regErr")).nativeElement;
            var pass = fixture.debugElement.query(platform_browser_1.By.css("#pass")).nativeElement;
            pass.value = "Test1";
            pass.dispatchEvent(new Event("input"));
            var cpass = fixture.debugElement.query(platform_browser_1.By.css("#cpass")).nativeElement;
            cpass.value = "Test2";
            cpass.dispatchEvent(new Event("input"));
            fixture.nativeElement.querySelector("#signUp").click();
            fixture.whenStable().then(function () {
                expect(p.textContent).toEqual("Passwords do not match");
            });
        });
    });
    describe("onKeypress", function () {
        it("should invoke signUp on keypress", testing_1.inject([auth_service_1.AuthService], function (authService) {
            var input = fixture.debugElement.query(platform_browser_1.By.css("#cpass"));
            input.triggerEventHandler("keyup.enter", {});
            fixture.detectChanges();
            expect(authService.signUp).toHaveBeenCalledTimes(1);
        }));
    });
});
//# sourceMappingURL=register.spec.js.map