"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var platform_browser_1 = require("@angular/platform-browser");
var nav_component_1 = require("../../nav/nav.component");
var auth_service_1 = require("../../services/auth.service");
// First, initialize the Angular testing environment.
testing_1.getTestBed().initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
var MockAuthService = (function () {
    function MockAuthService() {
        this.signOut = jasmine.createSpy("signOut");
    }
    return MockAuthService;
}());
describe("Component: NavComponent", function () {
    var component;
    var fixture;
    //updated beforeEach
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [nav_component_1.NavComponent],
            providers: [
                { provide: auth_service_1.AuthService, useClass: MockAuthService }
            ]
        }).compileComponents().then(function () {
            fixture = testing_1.TestBed.createComponent(nav_component_1.NavComponent);
            component = fixture.componentInstance;
        });
        // Mock localStorage
        var store = {};
        spyOn(localStorage, "getItem").and.callFake(function (key) {
            return store[key] || null;
        });
        spyOn(localStorage, "removeItem").and.callFake(function (key) {
            delete store[key];
        });
        spyOn(localStorage, "setItem").and.callFake(function (key, value) {
            return store[key] = value;
        });
        spyOn(localStorage, "clear").and.callFake(function () {
            store = {};
        });
        localStorage.setItem("userName", "Test");
    }));
    //tests
    describe("onCreate", function () {
        it("should display a different name", function () {
            var de = fixture.debugElement.query(platform_browser_1.By.css(".left"));
            var el = de.nativeElement;
            fixture.detectChanges();
            expect(el.textContent).toContain(localStorage.getItem("userName"));
        });
    });
    describe("#lOut (click)", function () {
        it('should call signOut when localStorage get item "access_token"', testing_1.inject([auth_service_1.AuthService], function (authService) {
            localStorage.setItem("access_token", "access_token");
            fixture.detectChanges();
            fixture.nativeElement.querySelector("#lOut").click();
            expect(authService.signOut).toHaveBeenCalledTimes(1);
        }));
        it('should not call signOut when localStorage get item "access_token"', testing_1.inject([auth_service_1.AuthService], function (authService) {
            fixture.detectChanges();
            fixture.nativeElement.querySelector("#lOut").click();
            expect(authService.signOut).toHaveBeenCalledTimes(0);
        }));
    });
});
//# sourceMappingURL=nav.spec.js.map