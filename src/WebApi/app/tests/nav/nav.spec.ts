import { ComponentFixture, async, inject, TestBed, getTestBed } from "@angular/core/testing";
import {
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting
    } from "@angular/platform-browser-dynamic/testing";
import { By } from "@angular/platform-browser";
import { NavComponent } from "../../nav/nav.component";
import { AuthService } from "../../services/auth.service";

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting()
);

class MockAuthService {
    signOut = jasmine.createSpy("signOut");
}

describe("Component: NavComponent",
    () => {
        let component: NavComponent;
        let fixture: ComponentFixture<NavComponent>;

        //updated beforeEach
        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [NavComponent],
                providers: [
                    { provide: AuthService, useClass: MockAuthService }
                ]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(NavComponent);
                component = fixture.componentInstance;
            });

            // Mock localStorage
            var store = {};

            spyOn(localStorage, "getItem").and.callFake((key: string): String => {
                return store[key] || null;
            });
            spyOn(localStorage, "removeItem").and.callFake((key: string): void => {
                delete store[key];
            });
            spyOn(localStorage, "setItem").and.callFake((key: string, value: string): string => {
                return store[key] = (value as string);
            });
            spyOn(localStorage, "clear").and.callFake(() => {
                store = {};
            });

            localStorage.setItem("userName", "Test");
        }));

        //tests
        describe("onCreate",
            () => {
                it("should display a different name",
                    () => {
                        const de = fixture.debugElement.query(By.css(".left"));
                        const el = de.nativeElement;
                        fixture.detectChanges();
                        expect(el.textContent).toContain(localStorage.getItem("userName"));
                    });
            });

        describe("#lOut (click)",
            () => {
                it('should call signOut when localStorage get item "access_token"',
                    inject([AuthService],
                        (authService: AuthService) => {
                            localStorage.setItem("access_token", "access_token");
                            fixture.detectChanges();
                            fixture.nativeElement.querySelector("#lOut").click();
                            expect(authService.signOut).toHaveBeenCalledTimes(1);
                        }));

                it('should not call signOut when localStorage get item "access_token"',
                    inject([AuthService],
                        (authService: AuthService) => {
                            fixture.detectChanges();
                            fixture.nativeElement.querySelector("#lOut").click();
                            expect(authService.signOut).toHaveBeenCalledTimes(0);
                        }));
            });
    });