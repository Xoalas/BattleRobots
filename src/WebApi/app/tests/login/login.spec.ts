﻿import { FormsModule } from "@angular/forms";
import { ComponentFixture, async, inject, TestBed, getTestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { By } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { LoginComponent } from "../../login/login.component";
import { AuthService } from "../../services/auth.service";

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting()
);

class MockRouter {
    navigate = jasmine.createSpy("navigate");
}

class MockAuthService {
    signIn = jasmine.createSpy("signIn");
}

describe("Component: LoginComponent",
    () => {
        let component: LoginComponent;
        let fixture: ComponentFixture<LoginComponent>;

        //updated beforeEach
        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [
                    RouterTestingModule, FormsModule
                ],
                declarations: [LoginComponent],
                providers: [
                    { provide: Router, useClass: MockRouter },
                    { provide: AuthService, useClass: MockAuthService }
                ]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(LoginComponent);
                component = fixture.componentInstance;
            });
        }));

        //tests
        describe("onKeypress",
            () => {
                it("should invoke signIn on keypress",
                    inject([AuthService],
                        (authService: AuthService) => {
                            const input = fixture.debugElement.query(By.css("#pass"));
                            input.triggerEventHandler("keyup.enter", {});
                            fixture.detectChanges();
                            expect(authService.signIn).toHaveBeenCalledTimes(1);
                        }));
            });

        describe("(click)",
            () => {
                it("should call signUp on click",
                    inject([Router],
                        (router: Router) => {
                            fixture.nativeElement.querySelector("#signUp").click();
                            fixture.detectChanges();
                            expect(router.navigate).toHaveBeenCalledWith([{ outlets: { primary: ["register"] } }],
                                { skipLocationChange: true });
                        }));

                it("should call signIn on click",
                    inject([Router, AuthService],
                        (router: Router, service: AuthService) => {
                            fixture.nativeElement.querySelector("#signIn").click();
                            fixture.detectChanges();
                            expect(service.signIn).toHaveBeenCalledTimes(1);
                        }));
            });
    });