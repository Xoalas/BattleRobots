"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var forms_1 = require("@angular/forms");
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var testing_3 = require("@angular/router/testing");
var login_component_1 = require("../../login/login.component");
var auth_service_1 = require("../../services/auth.service");
// First, initialize the Angular testing environment.
testing_1.getTestBed().initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
var MockRouter = (function () {
    function MockRouter() {
        this.navigate = jasmine.createSpy("navigate");
    }
    return MockRouter;
}());
var MockAuthService = (function () {
    function MockAuthService() {
        this.signIn = jasmine.createSpy("signIn");
    }
    return MockAuthService;
}());
describe("Component: LoginComponent", function () {
    var component;
    var fixture;
    //updated beforeEach
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                testing_3.RouterTestingModule, forms_1.FormsModule
            ],
            declarations: [login_component_1.LoginComponent],
            providers: [
                { provide: router_1.Router, useClass: MockRouter },
                { provide: auth_service_1.AuthService, useClass: MockAuthService }
            ]
        }).compileComponents().then(function () {
            fixture = testing_1.TestBed.createComponent(login_component_1.LoginComponent);
            component = fixture.componentInstance;
        });
    }));
    //tests
    describe("onKeypress", function () {
        it("should invoke signIn on keypress", testing_1.inject([auth_service_1.AuthService], function (authService) {
            var input = fixture.debugElement.query(platform_browser_1.By.css("#pass"));
            input.triggerEventHandler("keyup.enter", {});
            fixture.detectChanges();
            expect(authService.signIn).toHaveBeenCalledTimes(1);
        }));
    });
    describe("(click)", function () {
        it("should call signUp on click", testing_1.inject([router_1.Router], function (router) {
            fixture.nativeElement.querySelector("#signUp").click();
            fixture.detectChanges();
            expect(router.navigate).toHaveBeenCalledWith([{ outlets: { primary: ["register"] } }], { skipLocationChange: true });
        }));
        it("should call signIn on click", testing_1.inject([router_1.Router, auth_service_1.AuthService], function (router, service) {
            fixture.nativeElement.querySelector("#signIn").click();
            fixture.detectChanges();
            expect(service.signIn).toHaveBeenCalledTimes(1);
        }));
    });
});
//# sourceMappingURL=login.spec.js.map