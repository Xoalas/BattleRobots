"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configuration = {
    baseUrls: {
        server: "/",
        signalr: "http://www.battlerobots.somee.com"
    },
    environment: {
        production: true
    }
};
//# sourceMappingURL=app.constants.js.map