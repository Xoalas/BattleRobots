"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("../services/auth.service");
var registerForm_1 = require("../models/registerForm");
var RegisterComponent = (function () {
    function RegisterComponent(authService) {
        this.authService = authService;
        this.form = new registerForm_1.RegisterForm();
    }
    RegisterComponent.prototype.signUp = function () {
        if (this.form.Password !== this.form.ConfirmPassword) {
            $("#regErr").text("Passwords do not match").show();
            setTimeout(function () { $("#loginErr").hide(); }, 5000);
        }
        else
            this.authService.signUp("api/Account/Register", JSON.stringify(this.form));
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    core_1.Component({
        selector: "register",
        styleUrls: ["app/register/register.component.css"],
        template: "\n<div class=\"outer\">\n    <p id=\"regErr\"></p>\n    <div class=\"inner\">\n        <div class=\"elements\">\n            <input [(ngModel)]=\"form.UserName\" type=\"text\" class=\"text\" placeholder=\"Username\" />\n            <input [(ngModel)]=\"form.Password\" id=\"pass\" type=\"password\" class=\"text\" placeholder=\"Password\" />\n            <input (keyup.enter)=\"signUp()\" [(ngModel)]=\"form.ConfirmPassword\" id=\"cpass\" type=\"password\" class=\"text\" placeholder=\"Confirm Password\" />\n            <button (click)=\"signUp()\" id=\"signUp\" class=\"button\">SIGN UP</button>\n        </div>\n    </div>\n</div>"
    }),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], RegisterComponent);
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map