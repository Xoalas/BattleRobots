﻿import { Component } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { RegisterForm } from "../models/registerForm";

@Component({
    selector: "register",
    styleUrls: [`app/register/register.component.css`],
    template: `
<div class="outer">
    <p id="regErr"></p>
    <div class="inner">
        <div class="elements">
            <input [(ngModel)]="form.UserName" type="text" class="text" placeholder="Username" />
            <input [(ngModel)]="form.Password" id="pass" type="password" class="text" placeholder="Password" />
            <input (keyup.enter)="signUp()" [(ngModel)]="form.ConfirmPassword" id="cpass" type="password" class="text" placeholder="Confirm Password" />
            <button (click)="signUp()" id="signUp" class="button">SIGN UP</button>
        </div>
    </div>
</div>`
})
export class RegisterComponent {
    private form = new RegisterForm();

    constructor(private authService: AuthService) {}

    private signUp() {
        if (this.form.Password !== this.form.ConfirmPassword) {
            $("#regErr").text("Passwords do not match").show();
            setTimeout(() => { $("#loginErr").hide(); }, 5000);
        } else
            this.authService.signUp("api/Account/Register", JSON.stringify(this.form));
    }
}