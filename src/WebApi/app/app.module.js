"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_routing_module_1 = require("./app.routing.module");
//services
var http_service_1 = require("./services/http.service");
var auth_service_1 = require("./services/auth.service");
var chat_service_1 = require("./services/chat.service");
var prChat_service_1 = require("./services/prChat.service");
var game_service_1 = require("./services/game.service");
var searchGame_service_1 = require("./services/searchGame.service");
var redirectToGame_service_1 = require("./services/redirectToGame.service");
var contact_service_1 = require("./services/contact.service");
var mapper_service_1 = require("./services/mapper.service");
var logGame_service_1 = require("./services/logGame.service");
//components
var app_component_1 = require("./app.component");
var login_component_1 = require("./login/login.component");
var register_component_1 = require("./register/register.component");
var nav_component_1 = require("./nav/nav.component");
var contact_component_1 = require("./contact/contact.component");
var chat_component_1 = require("./chat/chat.component");
var privateChat_component_1 = require("./chat/privateChat.component");
var gameInit_component_1 = require("./game/gameInit.component");
var gameAction_component_1 = require("./game/gameAction.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, app_routing_module_1.AppRoutingModule],
        declarations: [
            app_component_1.AppComponent, login_component_1.LoginComponent, register_component_1.RegisterComponent, nav_component_1.NavComponent, contact_component_1.ContactComponent,
            chat_component_1.ChatComponent, privateChat_component_1.PrivateChatComponent, gameInit_component_1.GameInitComponent, gameAction_component_1.GameActionComponent
        ],
        providers: [
            http_service_1.HttpService, auth_service_1.AuthService, prChat_service_1.PrivateChatService, game_service_1.GameService, mapper_service_1.MapperService,
            searchGame_service_1.SearchGameService, redirectToGame_service_1.RedirectToGame, chat_service_1.ChatService, contact_service_1.ContactService, logGame_service_1.LogGameService
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map