"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_service_1 = require("../services/http.service");
var mapper_service_1 = require("../services/mapper.service");
var prChat_service_1 = require("../services/prChat.service");
var router_1 = require("@angular/router");
var PrivateChatComponent = (function () {
    function PrivateChatComponent(httpService, mapperService, aRouter, service) {
        this.httpService = httpService;
        this.mapperService = mapperService;
        this.aRouter = aRouter;
        this.service = service;
        this.map = new Map();
        this.messages = [];
        this.opponentId = localStorage.getItem("opponentUserId");
        this.id = this.aRouter.snapshot.params["id"];
        this.selfId = localStorage.getItem("userId");
    }
    PrivateChatComponent.prototype.ngOnInit = function () {
        localStorage.setItem("chatId", this.id);
        this.map = this.mapperService.mapIdToNameInActiveChat();
        this.getMessages();
        this.service.init(this.map);
        this.service.start();
    };
    PrivateChatComponent.prototype.getMessages = function () {
        var _this = this;
        this.httpService.get("api/chats/" + this.id + "/messages").subscribe(function (data) { return _this.messages = data; });
    };
    PrivateChatComponent.prototype.handle = function (event) {
        this.httpService.post("api/chats/" + this.id + "/send", JSON.stringify(event.target.value));
    };
    PrivateChatComponent.prototype.ngOnDestroy = function () {
        localStorage.removeItem("chatId");
        this.service.stop();
    };
    return PrivateChatComponent;
}());
PrivateChatComponent = __decorate([
    core_1.Component({
        selector: "privateChat",
        styleUrls: ["app/chat/chat.component.css"],
        template: "\n<div class=\"chatOuter\">\n    <div class=\"contentContainer\">\n        <div class=\"chatContentOverlay\"></div>\n        <div id=\"log\" class=\"chatContent\">\n            <ng-container *ngFor=\"let message of messages\">            \n                <div [ngStyle]=\"{'text-align': (message.UserId === selfId)? 'right' : 'left'}\">{{map.get(message.UserId)}}: {{message.Text}}</div><br>\n            </ng-container>\n        </div>\n    </div>\n    <div class=\"chatFooter\">\n        <input type=\"text\" (keyup.enter)=\"handle($event)\" id=\"msg\" class=\"txt\" placeholder=\"\tWrite a message...\">\n    </div>\n</div>",
    }),
    __metadata("design:paramtypes", [http_service_1.HttpService,
        mapper_service_1.MapperService,
        router_1.ActivatedRoute,
        prChat_service_1.PrivateChatService])
], PrivateChatComponent);
exports.PrivateChatComponent = PrivateChatComponent;
//# sourceMappingURL=privateChat.component.js.map