﻿import { Component, OnInit, OnDestroy } from "@angular/core";
import { HttpService } from "../services/http.service";
import { MapperService } from "../services/mapper.service";
import { PrivateChatService } from "../services/prChat.service";
import { Message } from "../models/message";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "privateChat",
    styleUrls: [`app/chat/chat.component.css`],
    template: `
<div class="chatOuter">
    <div class="contentContainer">
        <div class="chatContentOverlay"></div>
        <div id="log" class="chatContent">
            <ng-container *ngFor="let message of messages">            
                <div [ngStyle]="{'text-align': (message.UserId === selfId)? 'right' : 'left'}">{{map.get(message.UserId)}}: {{message.Text}}</div><br>
            </ng-container>
        </div>
    </div>
    <div class="chatFooter">
        <input type="text" (keyup.enter)="handle($event)" id="msg" class="txt" placeholder="\tWrite a message...">
    </div>
</div>`,
})
export class PrivateChatComponent implements OnInit, OnDestroy {
    private map = new Map<string, string>();
    private messages: Message[] = [];
    private opponentId = localStorage.getItem("opponentUserId");
    private id: string = this.aRouter.snapshot.params["id"];
    private selfId = localStorage.getItem("userId");

    constructor(private httpService: HttpService,
        private mapperService: MapperService,
        private aRouter: ActivatedRoute,
        private service: PrivateChatService) {
    }

    ngOnInit() {
        localStorage.setItem("chatId", this.id);
        this.map = this.mapperService.mapIdToNameInActiveChat();
        this.getMessages();
        this.service.init(this.map);
        this.service.start();
    }

    private getMessages() {
        this.httpService.get(`api/chats/${this.id}/messages`).subscribe(data => this.messages = data);
    }

    private handle(event: any) {
        this.httpService.post(`api/chats/${this.id}/send`, JSON.stringify(event.target.value));
    }

    ngOnDestroy() {
        localStorage.removeItem("chatId");
        this.service.stop();
    }
}