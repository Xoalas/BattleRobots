﻿import { Component, OnInit, OnDestroy } from "@angular/core";
import { ChatService } from "../services/chat.service";

@Component({
    selector: "chat",
    styleUrls: [`app/chat/chat.component.css`],
    template: `
<div class="chatOuter">
    <div class="contentContainer">
        <div class="chatContentOverlay"></div>
        <div id="log" class="chatContent"></div>
    </div>
    <div class="chatFooter" id="testtttt">
        <input type="text" id="msg" class="txt" placeholder="Write a message...">
    </div>
</div>`,
})
export class ChatComponent implements OnInit, OnDestroy {
    constructor(private service: ChatService) {}

    ngOnInit() {
        this.service.start();
    }

    ngOnDestroy() {
        this.service.stop();
    }
}