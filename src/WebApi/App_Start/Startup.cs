﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using WebApi.App_Start;

[assembly: OwinStartup(typeof(Startup))]

namespace WebApi.App_Start
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR("/signalr", new HubConfiguration());
        }
    }
}