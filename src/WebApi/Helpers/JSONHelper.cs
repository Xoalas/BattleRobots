﻿using System;
using System.Web.Script.Serialization;

namespace WebApi.Helpers
{
    public static class JsonHelper
    {
        public static string ToJson(this object obj)
        {
            var serializer = new JavaScriptSerializer();
            try
            {
                return serializer.Serialize(obj);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}