﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace WebApi.Helpers
{
    /// <summary>
    ///     Api Exception
    /// </summary>
    [Serializable]
    [DataContract]
    public class ApiException : Exception, ICustomExceptions
    {
        private string _reasonPhrase = "ApiException";

        /// <summary>
        ///     Public constructor for Custom Exception
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errorDescription"></param>
        /// <param name="httpStatus"></param>
        public ApiException(int errorCode, string errorDescription, HttpStatusCode httpStatus)
        {
            ErrorCode = errorCode;
            ErrorDescription = errorDescription;
            HttpStatus = httpStatus;
        }

        [DataMember] public int ErrorCode { get; set; }

        [DataMember] public string ErrorDescription { get; set; }

        [DataMember] public HttpStatusCode HttpStatus { get; set; }

        [DataMember]
        public string ReasonPhrase
        {
            get => _reasonPhrase;

            set => _reasonPhrase = value;
        }
    }
}