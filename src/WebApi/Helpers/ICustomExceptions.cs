﻿using System.Net;

namespace WebApi.Helpers
{
    /// <summary>
    ///     ICustomExceptions Interface
    /// </summary>
    public interface ICustomExceptions
    {
        /// <summary>
        ///     ErrorCode
        /// </summary>
        int ErrorCode { get; set; }

        /// <summary>
        ///     ErrorDescription
        /// </summary>
        string ErrorDescription { get; set; }

        /// <summary>
        ///     HttpStatus
        /// </summary>
        HttpStatusCode HttpStatus { get; set; }

        /// <summary>
        ///     ReasonPhrase
        /// </summary>
        string ReasonPhrase { get; set; }
    }
}